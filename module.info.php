<?php

return [
    'name'        => 'Manta',
    'author'      => 'Skimia Agency',
    'description' => 'Interface Graphique Skimia OS Backend',
    'namespace'   => 'Skimia\\Manta',
];