/// <reference path="jquery.d.ts" />

interface JQuery {
    material_select():JQuery;
    tooltip(config:any = {}):JQuery;
}

declare var material_select:JQueryStatic;