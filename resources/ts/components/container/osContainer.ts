/// <reference path="../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{
        direction:string;
    }

    interface IAttrs extends framework.IDirectiveAttrs{
        gutter:string;
    }

    /**
     *  osContainer Directive
     */
    export class osContainer extends framework.osJqueryDirective {

        /**
         * AngularJS Directive members
         */
        public scope = {
            direction: '@',
            gutter: '@'
        }

        /**
         * Constructor
         */
        constructor(templates, $timeout){
            super(templates, $timeout);
        }

        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attributes:IAttrs) {
            super.link($scope, element, attributes);

            element.addClass('os-container');
            if(attributes.gutter) element.children().css('margin-right', attributes.gutter);

            switch ($scope.direction) {
                case 'row':
                    element.addClass('row');
                    break;
                case 'row-reverse':
                    element.addClass('row-reverse');
                    break;
                case 'column':
                    element.addClass('column');
                    break;
                case 'column-reverse':
                    element.addClass('column-reverse');
                    break;
                default:


                    console.error('os-container: invalid direction : '+$scope.direction+' mis par defaut en row');
                    element.addClass('row');
                    break;
            }
        }
    }
}
