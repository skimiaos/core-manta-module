/// <reference path="../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{

    }

    interface IAttrs extends framework.IDirectiveAttrs{
        osFlex:string;
    }

    /**
     *  osContainer Directive
     */
    export class osFlex extends framework.osJqueryDirective {

        /**
         * AngularJS Directive members
         */
        public restrict = 'A';

        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attributes:IAttrs) {
            super.link($scope, element, attributes);

            this.services.$timeout(() => {
                $(element).css('flex', attributes.osFlex);
            }, 0);
        }
    }
}
