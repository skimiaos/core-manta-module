/// <reference path="../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{

    }

    interface IAttrs extends framework.IDirectiveAttrs{

    }

    /**
     *  osInput Directive
     */
    export class osTaskbar extends framework.osJqueryDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osTaskbar.html');
        public transclude:boolean = true;
        public scope = {

        };

        /**
         * Constructor
         */
        constructor(templates, $timeout){
            super(templates, $timeout);
        }

        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attributes:IAttrs):void {
            super.link($scope, element, attributes);

            this.services.$timeout(() => {
                $(element).addClass('os-taskbar');
            },0);
        }
    }
}