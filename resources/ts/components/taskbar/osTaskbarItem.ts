/// <reference path="../../framework/directives/osJqueryDataloadDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{
        color:string;
        active:boolean;
    }

    interface IAttrs extends framework.IDirectiveAttrs{

    }

    /**
     *  osInput Directive
     */
    export class osTaskbarItem extends framework.osJqueryDataloadDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osTaskbarItem.html');
        public transclude:boolean = true;
        public scope = {
            color: '@',
            href: '@',
            icon: '@',
            active: '=',
            close:'&'
        };

        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attrs:IAttrs):void {
            super.link($scope, element, attrs);

            this.services.$q.all([
                this.loadData('active')
            ]).then(() => {
                this.services.$timeout(() => {
                    $scope.$watch('active', (value) => {
                        if(value) $(element).find('li').addClass($scope.color);
                        else $(element).find('li').removeClass($scope.color);
                    });
                },0);
            });

            this.services.$timeout(() => {
                $(element).addClass('os-taskbar-item');

                $(element).find('li').on('mouseenter', () => {
                    $(element).find('li').addClass($scope.color);
                    $(element).find('p').addClass('active');
                    $(element).find('.os-taskbar-item--close').css('visibility', 'visible');
                });

                $(element).find('li').on('mouseleave', () => {
                    if(!$scope.active) $(element).find('li').removeClass($scope.color);
                    $(element).find('p').removeClass('active');
                    $(element).find('.os-taskbar-item--close').css('visibility', 'hidden');
                });

                $(element).find('.os-taskbar-item--close').on('mouseenter', () => {
                    $(element).find('.os-taskbar-item--close').removeClass('mdi-navigation-close');
                    $(element).find('.os-taskbar-item--close').addClass('mdi-navigation-cancel');
                });

                $(element).find('.os-taskbar-item--close').on('mouseout', () => {
                    $(element).find('.os-taskbar-item--close').removeClass('mdi-navigation-cancel');
                    $(element).find('.os-taskbar-item--close').addClass('mdi-navigation-close');
                });
            },0);
        }
    }
}