/// <reference path="../../framework/directives/osDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{

    }

    interface IAttrs extends framework.IDirectiveAttrs{

    }

    /**
     *  osInput Directive
     */
    export class osTaskbarUserpanel extends framework.osJqueryDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osTaskbarUserpanel.html');
        public transclude:boolean = true;
        public scope = {
            color: '@',
            username: '='
        };

        /**
         * Constructor
         */
        constructor(templates, $timeout){
            super(templates, $timeout);
        }

        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attributes:IAttrs):void {
            super.link($scope, element, attributes);

            this.services.$timeout(() => {
                $(element).addClass('os-taskbar-userpanel');

                $(element).find('.os-taskbar-userpanel--btn').on('click', () => {
                    if($(element).find('.os-taskbar-userpanel--menu').hasClass('is-open'))
                        $(element).find('.os-taskbar-userpanel--menu').removeClass('is-open');
                    else
                        $(element).find('.os-taskbar-userpanel--menu').addClass('is-open');
                });
            },0);
        }
    }
}