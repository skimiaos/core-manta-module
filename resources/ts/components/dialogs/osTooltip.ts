/// <reference path="../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface InputScope extends framework.IDirectiveScope{
        attributes:string;
        model:any;
        name:string;
        placeholder:string;
        type:string;
    }

    interface InputAttrs extends framework.IDirectiveAttrs{
        attributes:string;
        model:any;
        name:string;
        placeholder:string;
        type:string;
    }

    /**
     *  osInput Directive
     */
    export class osToolbox extends framework.osJqueryDirective{

        /**
         * AngularJS Directive members
         */
        public scope = {
        };

        /**
         * Constructor
         */
        constructor(templates, $timeout){
            super(templates, $timeout);
        }

        /**
         * Link
         */
        public link($scope:InputScope, element:JQuery, attributes:InputAttrs):void {
            super.link($scope, element, attributes);
            this.services.$timeout(()=>{


                $('*[os-toolbox]').tooltip();

            },100);

            $scope.$on(
                "$destroy",
                function handleDestroyEvent() {
                    $('.material-tooltip').remove();

                    $('*[os-toolbox]').tooltip();
                }
            );

        }
    }
}