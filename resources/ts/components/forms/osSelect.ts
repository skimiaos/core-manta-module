/// <reference path="../../framework/directives/osJqueryDataloadDirective.ts"/>
/// <reference path="../../vendors/materializecss.d.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface SelectScope extends framework.IDirectiveScope{
        name:string;
        placeholder:string;
        options:any;
        model:any;
        optionsVars:string;
        valueVar?:string;
        textVar?:string;
        ngModel?:any;
    }

    /**
     *  osSelect Directive
     */
    export class osSelect extends framework.osJqueryDataloadDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osSelect.html');
        public transclude:boolean = true;

        public scope = {
            name: '@',
            options: '=',
            model: '=ngModel',
            placeholder: '@'
        };

        /**
         * Constructor
         */
        constructor(templates, $q, $timeout){
            super(templates, $q, $timeout);
        }

        /**
         * Link
         */
        public link($scope:SelectScope, element:JQuery, attributes:ng.IAttributes):void {
            super.link($scope, element, attributes);
            $scope.ng = angular;

            this.services.$q.all([
                this.loadData('model'),
                this.loadData('options'),
                this.loadData('placeholder')
            ]).then(() => {
                //console.log($scope.model);
                if($scope.model == null) $scope.model = '__placeholder';
                $scope.model = $scope.model.toString();
                console.log($scope);
                console.log($scope.ng.extend({'__placeholder':$scope.placeholder}, $scope.options));

                this.services.$timeout(() => {
                    $(element).addClass('os-select col s12');


                    /*$(element).find('option[value="?"]').remove();
                    $(element).find('select option:last-child')
                        .attr('disabled', 'disabled')
                        .prependTo($(element).children('select'));

                    if($scope.model){

                    }
                    else{
                        $(element).find('select option:first-child')
                            .attr('selected', 'selected');
                    }*/


                    $(element).children('select').material_select();
                },0);
            });
        }
    };
}
