/// <reference path="../../framework/directives/osJqueryDataloadDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface InputScope extends framework.IDirectiveScope{
        attributes:string;
        model:any;
        name:string;
        placeholder:string;
        type:string;
    }

    interface InputAttrs extends framework.IDirectiveAttrs{
        attributes:string;
        model:any;
        name:string;
        placeholder:string;
        type:string;
    }

    /**
     *  osInput Directive
     */
    export class osInput extends framework.osJqueryDataloadDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osInput.html');
        public transclude:boolean = true;
        public scope = {
            name: '@',
            type: '@',
            placeholder: '@',
            attributes: '@',
            model: '=ngModel',
            disabled: '=osDisabled'
        };

        /**
         * Constructor
         */
        constructor(templates, $q, $timeout){
            super(templates, $q, $timeout);
        }

        /**
         * Link
         */
        public link($scope:InputScope, element:JQuery, attributes:InputAttrs):void {
            super.link($scope, element, attributes);
            if(!attributes.type) $scope.type = 'text';
            $(element).addClass('os-input col s12');

            this.services.$q.all([
                this.loadData('model')
            ]).then(() => {
                this.services.$timeout(() => {




                    if($scope.disabled == undefined)
                        $scope.disabled = false;

                    if($scope.placeholder == undefined)
                        $(element).children('input').removeAttr('placeholder');
                    else
                        $(element).children('label').addClass('active');

                    if($scope.model == undefined)
                        $(element).children('input').removeAttr('ng-model');

                    if($scope.model != ''){
                        $(element).children('label').addClass('active');
                    }

                    /*this.services.$timeout(() => {
                        $(element).children('label').addClass('active');
                        $(element).children('input').trigger('blur');
                    },50);*/

                },1);



            });
        }
    }
}