module osml.controllers {
    export class SidenavController{
        private $scope:{
            hasMenu:boolean;
            color:string;
            $watch(string, Function);
        };

        constructor($scope, private $element:JQuery){
            this.$scope = $scope;
            this.$scope.hasMenu = false;

            this.$scope.$watch('hasMenu', (value) => {
                if(value) $($element).addClass('hasMenu');
                else $($element).removeClass('hasMenu');
            });
        }

        public openMenu(isOpen:boolean) {
            this.$scope.hasMenu = isOpen;
            this.$scope.$parent.$broadcast('sidenavopenchanged',isOpen);
            if(this.$scope.hasMenu) $(this.$element).addClass('hasMenu');
            else $(this.$element).removeClass('hasMenu');
        }

        public getColor():string {
            return this.$scope.color;
        }

        public removeActive():void {
            $(this.$element).find('.os-sidenav-item.active').removeClass('active');
            $(this.$element).find('.os-sidenav-menu.isOpen').removeClass('isOpen');
            $(this.$element).find('.os-sidenav-browser.isOpen').removeClass('isOpen');
            $(this.$element).find('.os-sidenav-item--link p').removeClass('hidden');
            this.openMenu(false);
        }
    }
}

