/// <reference path="../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{

    }

    interface IAttrs extends framework.IDirectiveAttrs{
        color:string;
    }

    /**
     *  osInput Directive
     */
    export class osSidenav extends framework.osJqueryDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osSidenav.html');
        public transclude:boolean = true;
        public controller = controllers.SidenavController;
        public scope = {
            color: '@',
            label: '@'
        };


        /**
         * Constructor
         */
        constructor(templates, $timeout){
            super(templates, $timeout);
        }

        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attrs:IAttrs):void {
            super.link($scope, element, attrs);



            this.services.$timeout(() => {
                $(element).addClass('os-sidenav');
            },0);
        }
    }
}