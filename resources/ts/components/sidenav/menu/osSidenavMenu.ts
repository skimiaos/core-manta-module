/// <reference path="../../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{
        title:string;
    }

    interface IAttrs extends framework.IDirectiveAttrs{

    }

    /**
     *  osInput Directive
     */
    export class osSidenavMenu extends framework.osJqueryDataloadDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osSidenavMenu.html');
        public transclude:boolean = true;
        public controller = controllers.SidenavMenuController;
        public scope = {
            title: '@'
        };

        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attrs:IAttrs):void {
            super.link($scope, element, attrs);

            this.services.$timeout(() => {
                $(element).addClass('os-sidenav-menu');
                $scope.title = $(element).parents('os-sidenav-item').children('li').find('p').text();
            },0);
        }
    }
}