/// <reference path="../../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{

    }

    interface IAttrs extends framework.IDirectiveAttrs{

    }

    /**
     *  osInput Directive
     */
    export class osSidenavMenuItem extends framework.osJqueryDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osSidenavMenuItem.html');
        public transclude:boolean = true;
        public require = '^osSidenavMenu';
        public scope = {
            name: '@',
            icon: '@'
            //active: '='
        };

        /**
         * Constructor
         */
        constructor(templates, $timeout){
            super(templates, $timeout);
        }

        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attrs:IAttrs):void {
            super.link($scope, element, attrs);

            if($scope.active == undefined) $scope.active = false;

            this.services.$timeout(() => {
                $(element).addClass('os-sidenav-menu-item');
            },0);
        }

        private registerEvents(element:JQuery, sidenavMenuCtrl:controllers.SidenavMenuController):void {
            $(element).on('click', () => {
                if(!$(element).hasClass('active')){
                    sidenavMenuCtrl.removeActive();
                    $(element).addClass('active');
                }
            });
        }

    }
}