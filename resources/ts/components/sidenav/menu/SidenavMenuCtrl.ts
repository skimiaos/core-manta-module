module osml.controllers {
    export class SidenavMenuController{
        private $scope:{
            $watch(string, Function);
        };

        constructor($scope, private $element:JQuery){
            this.$scope = $scope;
        }

        public removeActive():void {
            $(this.$element).removeClass('active');
        }
    }
}

