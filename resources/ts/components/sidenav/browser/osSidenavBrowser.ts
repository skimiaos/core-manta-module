/// <reference path="../../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{
        title:string;
        model:any;
        directory:any;
        openFile:any;
        current_directory:any;
        parent_directory:any;
        history:any;
        breadcrumb:any;
        parent:any;
        search:any;
        events:any;
        addFile:any;
    }

    interface IAttrs extends framework.IDirectiveAttrs{

    }

    /**
     *  osInput Directive
     */
    export class osSidenavBrowser extends framework.osJqueryDataloadDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osSidenavBrowser.html');
        //public controller = controllers.SidenavBrowserController;
        public scope = {
            model: '=ngModel',
            events: '=?'
        };

        constructor($template, $q, $timeout){
            super($template, $q, $timeout);
            this.isFile = this.isFile.bind(this);
        }
        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attrs:IAttrs):void {
            super.link($scope, element, attrs);

            var load = ($auto_bread) => {
                $scope.current_directory = this.adaptModel($scope.model);
                $scope.history = [];
                $scope.breadcrumb = [];
                $scope.directory = 'racine';

                this.services.$timeout(() => {
                    $(element).addClass('os-sidenav-browser');
                    $scope.title = $(element).parents('os-sidenav-item').children('li').find('p').text();
                },0);

                $scope.addFile = function(){
                    if($scope.breadcrumb.length == 0){
                        return $scope.events.create('');
                    }else{
                        var crumb = [$scope.directory] + $scope.breadcrumb.slice(1);
                        return $scope.events.create(crumb.concat('/'));
                    }

                }
                $scope.openFile = (name)=>{
                    var file:any = $scope.current_directory[name];

                    if(file == false)
                        return;

                    if(this.isFile(file)){
                        $scope.events.open(file);
                    }else{
                        this.changeDirectory($scope,name);
                    }
                };
                $scope.parent = ()=>{
                    this.parentDirectory($scope);
                };

                angular.forEach($auto_bread, function(file){

                    $scope.openFile(file);
                });
            };

            this.services.$q.all([
                this.loadData('model')
            ]).then(load([]));

            $scope.$watch('model',(newValue,oldValue)=>{
                if($scope.breadcrumb.length == 0)
                    load([]);
                else{

                    load(angular.extend([$scope.directory],$scope.breadcrumb.slice(1)));
                }

            });
        }

        private isFile(file){
            if(!file) return false;
            if(angular.isDefined(file.__TYPE) && file.__TYPE == 'FILE') return true;
            return angular.isDefined(file.mime) && typeof file['mime'] == 'string';
        }

        private adaptModel(model:any):any{
            angular.forEach(model, (file, name) => {

                if(this.isFile(file)){
                    file['isDirectory'] = false;
                    file['icon'] = this.iconizeMimeType(file['mime']);
                    if(!angular.isDefined(file['info']))
                        file['info'] = file['size']+', Modifié le '+file['modified'];
                    file['_name'] = name;

                }

                else{
                    if(!angular.isDefined(file['childrens'])){
                        var f = angular.copy(file);
                        file = {
                            childrens:f
                        };
                    }

                    file['isDirectory'] = true;
                    file['icon'] = 'mdi-file-folder';
                    file['directories'] = 0;
                    file['files'] = 0;
                    angular.forEach(file['childrens'],(sub_file,name)=>{
                        if(this.isFile(sub_file))
                            file['files'] ++ ;
                        else
                            file['directories']++;

                    });

                    file['info'] = '';
                    if(file['directories'] > 0)
                        file['info'] = file['directories']+' répertoire'+(file['directories'] > 1 ? 's':'');

                    if(file['files'] > 0)
                        file['info'] += (file['directories'] > 0?', ':'')+file['files']+' fichier'+(file['files'] > 1 ? 's':'');
                    file['_name'] = name;

                }
                model[name] = file;

            });

            return model;
        }

        private iconizeMimeType(mime:string){
            var mimes:string[] = mime.split('/');
            switch (mimes[0]){
                case 'image':

                    return 'mdi-image-crop-original';
                    break;
                case 'text':
                    return 'mdi-editor-format-align-left';
                    break;
            }
            return 'os-icon-help-1';
        }

        private openFile(name:string){

        }

        public $scope:IScope;

        private changeDirectory($scope:any,name:any){
            $scope.history.push($scope.current_directory);
            $scope.breadcrumb.push($scope.directory);

            $scope.current_directory = this.adaptModel($scope.current_directory[name]['childrens']);
            $scope.directory = name;
        }

        private parentDirectory($scope){

            $scope.current_directory = $scope.history.pop();
            $scope.directory = $scope.breadcrumb.pop();

        }
    }
}