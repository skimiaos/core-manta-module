/// <reference path="../../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{

    }

    interface IAttrs extends framework.IDirectiveAttrs{

    }

    /**
     *  osInput Directive
     */
    export class osSidenavBrowserItem extends framework.osJqueryDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osSidenavBrowserItem.html');
        public transclude:boolean = true;
        public scope = {
            name: '@',
            icon: '@'
        };

        /**
         * Constructor
         */
        constructor(templates, $timeout){
            super(templates, $timeout);
        }

        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attrs:IAttrs):void {
            super.link($scope, element, attrs);

            this.services.$timeout(() => {
                $(element).addClass('os-sidenav-browser-item');
            },0);
        }
    }
}