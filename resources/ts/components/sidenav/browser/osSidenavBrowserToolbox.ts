/// <reference path="../../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{
        query:any;
    }

    interface IAttrs extends framework.IDirectiveAttrs{

    }

    /**
     *  osInput Directive
     */
    export class osSidenavBrowserToolbox extends framework.osJqueryDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osSidenavBrowserToolbox.html');
        public transclude:boolean = true;
        public scope = {
            name: '@',
            icon: '@',
            returnClick: '&',
            search: '&',
            showReturn: '=',
            add:'&'
        };
        //public require = '^osSidenavBrowser';

        /**
         * Constructor
         */
        constructor(templates, $timeout){
            super(templates, $timeout);
        }


        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attrs:IAttrs/*, sidenavBrowserCtrl:controllers.SidenavBrowserController*/):void {
            super.link($scope, element, attrs);

            $scope.query = {text:''};
            /*$scope.$watch('query.text',(newValue,oldValue)=>{
                if(newValue == oldValue)
                    return;
                if(newValue =='')
                    sidenavBrowserCtrl.clearSearch();
                else
                    sidenavBrowserCtrl.search(newValue);
            });*/
            this.services.$timeout(() => {
                $(element).addClass('os-sidenav-browser-toolbox');

                $(element).find('.os-sidenav-browser-toolbox--search input').on('focus', () => {
                    $(element).find('.os-sidenav-browser-toolbox--search').addClass('isFocus');
                });

                $(element).find('.os-sidenav-browser-toolbox--search input').on('focusout', () => {
                    $(element).find('.os-sidenav-browser-toolbox--search').removeClass('isFocus');
                });

                $(element).find('.os-sidenav-browser-toolbox--search input').on('keyup', () => {
                    if($(element).find('.os-sidenav-browser-toolbox--search input').val() != ''){
                        $(element).find('.os-sidenav-browser-toolbox--search').addClass('hasText');
                    }
                    else{
                        $(element).find('.os-sidenav-browser-toolbox--search').removeClass('hasText');
                    }
                });

                $(element).find('.os-sidenav-browser-toolbox--search form > i').on('click', () => {
                    $(element).find('.os-sidenav-browser-toolbox--search').removeClass('isFocus');
                    $(element).find('.os-sidenav-browser-toolbox--search').removeClass('hasText');
                    $(element).find('.os-sidenav-browser-toolbox--search input').val('');
                });
            },0);
        }
    }
}