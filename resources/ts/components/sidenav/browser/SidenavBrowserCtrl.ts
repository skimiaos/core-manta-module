module osml.controllers {
    export class SidenavBrowserController{
        private $scope:{
            searchQuery:string;
            $watch(string, Function);
        };

        constructor($scope, private $element:JQuery){
            this.$scope = $scope;
        }

        public search(query:string){
            this.$scope.searchQuery = query;
        }

        public clearSearch(){
            this.$scope.searchQuery = '';
            console.log('fu');
        }
    }
}

