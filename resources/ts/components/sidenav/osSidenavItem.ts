/// <reference path="../../framework/directives/osJqueryDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface IScope extends framework.IDirectiveScope{
        active:boolean;
        transcluded:string;
        href:string;
        color:string;
    }

    interface IAttrs extends framework.IDirectiveAttrs{

    }

    /**
     *  osInput Directive
     */
    export class osSidenavItem extends framework.osJqueryDirective{

        /**
         * AngularJS Directive members
         */
        public template:string = this.services.$templates.get('osSidenavItem.html');
        public transclude:boolean = true;
        public require = '^osSidenav';
        public scope = {
            href: '@',
            icon: '@',
            color: '@?',
            label: '@',
            active: '='
        };

        public $scope:IScope;

        /**
         * Link
         */
        public link($scope:IScope, element:JQuery, attrs:IAttrs, sidenavCtrl:controllers.SidenavController):void {
            super.link($scope, element, attrs);
            this.registerEvents(element, sidenavCtrl);

            this.services.$timeout(() => {
                $(element).addClass('os-sidenav-item');

                if($scope.active) {
                    $(element).find('.os-sidenav-item--link').trigger('click');
                }
                if($scope.href) $(element).children('li').find('a').attr('href', $scope.href);

                $scope.color = angular.isDefined($scope.color) ? $scope.color : sidenavCtrl.getColor();



            },0);
        }

        private registerEvents(element:JQuery, sidenavCtrl:controllers.SidenavController):void {
            $(element).find('.os-sidenav-item--link').on('mouseenter', () => {
                $(element).addClass('hover');
            });

            $(element).find('.os-sidenav-item--link').on('mouseleave', () => {
                $(element).removeClass('hover');
            });

            $(element).find('.os-sidenav-item--link').on('click', () => {
                var submenu = $(element).find('.os-sidenav-item--submenu').children();


                var hClass = submenu.hasClass('isOpen');
                if(submenu.length || submenu.hasClass('isOpen')){
                    sidenavCtrl.openMenu(false);
                    $(element).find('.os-sidenav-item--link p').removeClass('hidden');
                    if(submenu.length)
                        submenu.removeClass('isOpen');
                    sidenavCtrl.removeActive();
                    //$(element).addClass('active');
                }
                if(!submenu.hasClass('isOpen') && submenu.length && !hClass){
                    $(element).removeClass('hover');

                    /*sidenavCtrl.removeActive();*/

                    sidenavCtrl.openMenu(true);
                    $(element).find('.os-sidenav-item--link p').addClass('hidden');
                    submenu.addClass('isOpen');
                    $(element).addClass('active');
                }
                else if(!submenu.length || submenu.hasClass('isOpen')){
                    sidenavCtrl.openMenu(false);
                    $(element).find('.os-sidenav-item--link p').removeClass('hidden');
                    if(submenu.length)
                        submenu.removeClass('isOpen');
                    sidenavCtrl.removeActive();
                    //$(element).addClass('active');
                }

            });
        }
    }
}