/// <reference path="../../framework/directives/osDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface InputScope extends framework.IDirectiveScope{
        attributes:string;
        model:any;
        name:string;
        placeholder:string;
        type:string;
        $watch:any;
        $eval:any;
        $parent:any;


    }

    interface InputAttrs extends framework.IDirectiveAttrs{
        osTabContentTransclude:string;
    }

    /**
     *  osInput Directive
     */
    export class osTabContentTransclude extends framework.osJqueryDataloadDirective{

        /**
         * AngularJS Directive members
         */
        public require:string     = '^?osTab';
        public restrict:string    = 'A';
        public scope = true;
        /**
         * Constructor
         */
        constructor(templates, $q, $timer){
            super(templates, $q, $timer);
        }

        public link($scope:InputScope, element:JQuery, attributes:InputAttrs):void {
            super.link($scope, element, attributes);

            var tab = $scope.$parent.$eval(attributes.osTabContentTransclude);

            var that = this;
            //Now our tab is ready to be transcluded: both the tab heading area
            //and the tab content area are loaded.  Transclude 'em both.
            tab.$transcludeFn(tab.$parent, function(contents) {
                angular.forEach(contents, (node)=> {
                    if (that.isTabHeading(node)) {
                        //Let tabHeadingTransclude know.
                        tab.headingElement = node;
                    } else {
                        element.append(node);
                    }
                });
            });
        }

        public isTabHeading(node) {
            return node.tagName &&  (
                node.hasAttribute('tab-heading') ||
                node.hasAttribute('data-tab-heading') ||
                node.tagName.toLowerCase() === 'tab-heading' ||
                node.tagName.toLowerCase() === 'data-tab-heading'
                );
        }
    }
}