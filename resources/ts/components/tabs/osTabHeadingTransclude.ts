/// <reference path="../../framework/directives/osDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface InputScope extends framework.IDirectiveScope{
        attributes:string;
        model:any;
        name:string;
        placeholder:string;
        type:string;
        $watch:any;


    }

    interface InputAttrs extends framework.IDirectiveAttrs{
        attributes:string;
        model:any;
        name:string;
        placeholder:string;
        type:string;
    }

    /**
     *  osInput Directive
     */
    export class osTabHeadingTransclude extends framework.osJqueryDataloadDirective{

        /**
         * AngularJS Directive members
         */
        public require:string     = '^osTab';
        public restrict:string    = 'A';


        public static $inject = ['$q', '$timeout'];
        /**
         * Constructor
         */
        constructor(templates, $q, $timer){
            super(templates, $q, $timer);
        }

        public link($scope:InputScope, element:JQuery, attributes:InputAttrs):void {
            super.link($scope, element, attributes);

            $scope.$watch('headingElement', function updateHeadingElement(heading) {
                if (heading) {
                    element.html('');
                    element.append(heading);
                }
            });
        }
    }
}
