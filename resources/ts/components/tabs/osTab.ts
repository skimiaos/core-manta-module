/// <reference path="../../framework/directives/osDirective.ts"/>

module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface InputScope extends framework.IDirectiveScope{
        attributes:string;
        model:any;
        name:string;
        placeholder:string;
        type:string;
    }

    interface InputAttrs extends framework.IDirectiveAttrs{
        attributes:string;
        model:any;
        name:string;
        placeholder:string;
        type:string;
    }

    /**
     *  osInput Directive
     */
    export class osTab extends framework.osJqueryDataloadDirective{

        /**
         * AngularJS Directive members
         */
        public require:string     = '^osTabset';
        public restrict:string    = 'EA';
        public replace:boolean    = true;
        public transclude:boolean = true;

        public template:string = this.services.$templates.get('osTab.html');

        public scope = {
            active: '=?',
            heading: '@',
            onSelect: '&select', //This callback is called in contentHeadingTransclude
            //once it inserts the tab's content into the dom
            onDeselect: '&deselect',
            close:'&',
            modified:'=?'
        };

        public static $inject = ['$q', '$timeout','$parse','$log'];
        /**
         * Constructor
         */
        constructor(templates, $q, $timer, $parse, $log){
            super(templates, $q, $timer);
            this.services['$parse'] = $parse
            this.services['$log'] = $log
        }


        public controller(){}

        public compile(elms, attrs, transclude) {
            var that = this;
            return function postLink(scope, elm, attrs, osTabsetCtrl) {

                if(!angular.isDefined(scope.modified)){
                    scope.modified = false;
                }
                scope.$watch('active', function(active) {
                    if (active) {

                        that.services.$timeout(()=>{elm.find('a').trigger('click');});
                        osTabsetCtrl.select(scope,elm);
                    }
                });

                scope.disabled = false;
                if ( attrs.disable ) {
                    scope.$parent.$watch(this.services.$parse(attrs.disable), function(value) {
                        scope.disabled = !! value;
                    });
                }

                // Deprecation support of "disabled" parameter
                // fix(tab): IE9 disabled attr renders grey text on enabled tab #2677
                // This code is duplicated from the lines above to make it easy to remove once
                // the feature has been completely deprecated
                if ( attrs.disabled ) {
                    this.services.$log.warn('Use of "disabled" attribute has been deprecated, please use "disable"');
                    scope.$parent.$watch(this.services.$parse(attrs.disabled), function(value) {
                        scope.disabled = !! value;
                    });
                }

                scope.select = function() {
                    if ( !scope.disabled ) {
                        scope.active = true;
                    }
                };

                osTabsetCtrl.addTab(scope);
                scope.$on('$destroy', function() {
                    osTabsetCtrl.removeTab(scope);
                });

                //We need to transclude later, once the content container is ready.
                //when this link happens, we're inside a tab heading.
                scope.$transcludeFn = transclude;
                scope.$element = elms;

                //Jquery Events
                that.services.$timeout(() => {
                    var width = $(elm).find('.tab').width() + 72;
                    $(elm).css('width', width + 'px');

                    $(elm).find('.tab i').on('mouseenter', () => {


                        $(elm).find('.tab i').removeClass('mdi-alert-error').removeClass('mdi-navigation-close');

                        $(elm).find('.tab i').addClass('mdi-navigation-cancel');
                    });

                    $(elm).find('.tab i').on('mouseout', () => {

                        $(elm).find('.tab i').removeClass('mdi-navigation-cancel');
                        if(scope.modified)
                            $(elm).find('.tab i').addClass('mdi-alert-error');
                        else
                            $(elm).find('.tab i').addClass('mdi-navigation-close');
                    });
                },0);
            };
        }

        public link($scope:InputScope, element:JQuery, attributes:InputAttrs):void {
            super.link($scope, element, attributes);


        }
    }
}
