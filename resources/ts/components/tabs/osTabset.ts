/// <reference path="../../framework/directives/osDirective.ts"/>


module osml.directives{
    'use strict';

    /**
     * Interfaces
     */
    interface InputScope extends framework.IDirectiveScope{
        vertical:boolean;
        justified:boolean;
    }

    interface InputAttrs extends framework.IDirectiveAttrs{
        vertical:boolean;
        justified:boolean;
        $parent:any;
    }

    /**
     *  osInput Directive
     */
    export class osTabset extends framework.osJqueryDataloadDirective{

        /**
         * AngularJS Directive members
         */
        public restrict:string    = 'EA';
        public transclude:boolean = true;

        public template:string = this.services.$templates.get('osTabset.html');

        public scope = {
            type: '@'
        };
        public controller:string = 'TabsetController'
        /**
         * Constructor
         */
        constructor(templates, $q, $timer){
            super(templates, $q, $timer);
        }



        public link($scope:InputScope, element:JQuery, attributes:InputAttrs):void {
            super.link($scope, element, attributes);
            $(element).addClass('os-tabset');
            $scope.vertical = angular.isDefined(attributes.vertical) ? attributes.$parent.$eval(attributes.vertical) : false;
            $scope.justified = angular.isDefined(attributes.justified) ? attributes.$parent.$eval(attributes.justified) : false;
        }
    }
}




module osml.controllers {
    export class TabsetController {
        public static $inject:string[] = ["$scope"];

        public tabs:osml.directives.osTab[];
        public destroyed:boolean = false;

        constructor (private $scope) {
            this.tabs = $scope.tabs = [];

            $scope.$on('$destroy', function() {
                this.destroyed = true;
            });
        }

        public select(selectedTab) {


            angular.forEach(this.tabs, function(tab:any) {
                if (tab.active && tab !== selectedTab) {
                    tab.active = false;
                    tab.onDeselect();
                }

            });
            selectedTab.active = true;
            selectedTab.onSelect();

        }

        public addTab(tab) {
            this.tabs.push(tab);
            // we can't run the select function on the first tab
            // since that would select it twice
            if (this.tabs.length === 1 && tab.active !== false) {
                tab.active = true;
            } else if (tab.active) {
                this.select(tab);
            }
            else {
                tab.active = false;
            }
            $('ul.tabs').tabs();
        }

        public removeTab(tab) {
            var index = this.tabs.indexOf(tab);
            //Select a new tab if the tab to be removed is selected and not destroyed
            if (tab.active && this.tabs.length > 1 && !this.destroyed) {
                //If this is the last tab, select the previous tab. else, the next tab.
                var newActiveIndex = index == this.tabs.length - 1 ? index - 1 : index + 1;
                this.select(this.tabs[newActiveIndex]);
            }
            this.tabs.splice(index, 1);
            $('ul.tabs').tabs();
        }

    }
}
