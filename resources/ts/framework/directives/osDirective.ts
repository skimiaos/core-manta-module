module osml.framework{
    'use strict';

    /**
     * Interfaces
     */
    export interface IDirectiveScope{
        $watch(watchExpression: string, listener?: (newValue: any, oldValue: any, scope: ng.IScope) => any, objectEquality?: boolean): Function;
    }
    export interface IDirectiveAttrs{}
    export interface IDirectiveServices{
        $templates: {
            get(string);
        };
    }

    /**
     *  AngularJS Base directive
     */
    export class osDirective implements ng.IDirective{

        /**
         * Dependencies injection
         */
        public static $inject:string[] = [];
        public static baseInject:string[] = ['$templateCache'];

        public static injector():string[] {
            return this.baseInject.concat(this.$inject);
        }

        /**
         * Scope & Services
         */
        public $scope:IDirectiveScope;
        public services:IDirectiveServices;

        /**
         * Constructor
         */
        constructor(templateCache){
            this.link = this.link.bind(this);
            this.services = {
                $templates: templateCache
            };
        }

        /**
         * Link
         */
        public link($scope:IDirectiveScope, element:JQuery, attributes:IDirectiveAttrs, ...dependencies):void {
            this.$scope = $scope;
        }

        /**
         * Factory
         */
        public static Factory():any {
            var directive = (...args) => this.construct(this.valueOf(), args);
            directive.$inject = this.injector();
            return directive;
        }

        public static construct(constructor:any, args:any):any {
            function F() : void {
                constructor.apply(this, args);
            }
            F.prototype = constructor.prototype;
            return new F();
        }
    }
}