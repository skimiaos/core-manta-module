/// <reference path="../../framework/directives/osDataloadDirective.ts"/>
/// <reference path="../../framework/directives/osJqueryDirective.ts"/>

module osml.framework{
    'use strict';

    /**
     * Interfaces
     */
    export interface IJqueryDataloadDirectiveServices extends IDataloadDirectiveServices, IJqueryDirectiveServices{}

    /**
     *  AngularJS Base directive
     */
    export class osJqueryDataloadDirective extends osDataloadDirective{

        /**
         * Dependencies injection
         */
        public static jqueryInject:string[] = ['$timeout'];

        public static injector():string[] {
            var inject:string[] = this.baseInject.concat(this.dataloadInject);
            inject = inject.concat(this.jqueryInject);
            return inject.concat(this.$inject);
        }

        /**
         * Scope & Services
         */
        public services:IJqueryDataloadDirectiveServices;

        /**
         * Constructor
         */
        constructor(templates, $q, $timeout){
            super(templates, $q)
            this.services['$timeout'] = $timeout;
        }
    }
}