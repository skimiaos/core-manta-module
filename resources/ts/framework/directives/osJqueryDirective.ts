/// <reference path="../../framework/directives/osDirective.ts"/>

module osml.framework{
    'use strict';

    /**
     * Interfaces
     */
    export interface IJqueryDirectiveServices extends IDirectiveServices{
        $timeout:any;
    }

    /**
     *  AngularJS Base directive
     */
    export class osJqueryDirective extends osDirective{

        /**
         * Dependencies injection
         */
        public static jqueryInject:string[] = ['$timeout'];

        public static injector():string[] {
            var inject:string[] = this.baseInject.concat(this.jqueryInject);
            return inject.concat(this.$inject);
        }

        /**
         * Scope & Services
         */
        public services:IJqueryDirectiveServices;

        /**
         * Constructor
         */
        constructor(templates, $timeout){
            super(templates);
            this.services['$timeout'] = $timeout;
        }
    }
}