/// <reference path="../../framework/directives/osDirective.ts"/>

module osml.framework{
    'use strict';

    /**
     * Interfaces
     */
    export interface IDataloadDirectiveServices extends IDirectiveServices {
        $q:any;
    }

    /**
     *  AngularJS Base directive
     */
    export class osDataloadDirective extends osDirective{

        /**
         * Dependencies injection
         */
        public static dataloadInject:string[] = ['$q'];

        public static injector():string[] {
            var inject:string[] = this.baseInject.concat(this.dataloadInject);
            return inject.concat(this.$inject);
        }

        /**
         * Scope & Services
         */
        public services:IDataloadDirectiveServices;

        /**
         * Constructor
         */
        constructor(templates, $q){
            super(templates);
            this.services['$q'] = $q;
        }

        /**
         * Load trick for waiting data provide
         * @param scopeAttr
         * @returns {angular.IPromise<T>}
         */
        public loadData(scopeAttr):ng.IPromise<any> {
            var loading = this.services.$q.defer();
            if(this.$scope[scopeAttr]) {
                loading.resolve(this.$scope[scopeAttr]);
            } else {
                this.$scope.$watch(scopeAttr, function(newValue, oldValue) {
                    if(newValue !== undefined)
                        loading.resolve(newValue);
                });
            }
            return loading.promise;
        }
    }
}