module osml.framework{
    'use strict';

    /**
     *  AngularJS Base controller
     */
    export class osController{

        public static $inject:string[] = ['$scope'];
        public scope:ng.IScope;

        constructor($scope:ng.IScope){
            this.scope = $scope;
            this.scope['$events'] = this;
        }

    }
}