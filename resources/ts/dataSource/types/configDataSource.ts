/// <reference path="DataSource.ts"/>

module dataSource.types {
    export class ConfigDataSource extends dataSource.types.DataSource{


        protected subscribers = [];

        protected kvp = {};

        public listen($closure):any{
            this.subscribers.push($closure);

            return this;
        }

        public touch($key){
            angular.forEach(this.subscribers,(closure)=>{
                if(false == closure($key,this.kvp[$key]))
                    return;
            });

            return this;
        }

        public set($key,$value){
            var current = this.kvp[$key];
            if(current != $value){
                this.kvp[$key] = $value;
                this.touch($key);
            }
            return this;

        }

        public returnSet($key,$value){
            this.set($key,$value);

            return this.kvp[$key];
        }

        public get($key){
            if(angular.isDefined(this.kvp[$key]))
                return this.kvp[$key];
            console.error('Erreur dans le KVP la valeur '+$key+' n\'existe pas veuillez l\'initialiser a la mano');
        }

        public static provideDefault($source:ConfigDataSource,data:any){

            angular.forEach(data,(value,key)=>{
                $source.set(key,value);

            });
        }

    }
}