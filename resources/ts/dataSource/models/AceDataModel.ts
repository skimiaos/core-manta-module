/// <reference path="KVPDataModel.ts"/>

module dataSource.models {
    export class AceDataModel extends dataSource.models.KVPDataModel{


        public $sourceContext = 'ace-editor';
        public $config:any;

        public _editor:any;
        public _session:any;
        public _renderer:any;

        public $source:dataSource.types.ConfigDataSource;
        public aceOptions:any;


        constructor($dataSource:any,$config:any){
            super($dataSource);
            this.$source = $dataSource.source('kvp',this.$sourceContext);
            this.$config = $config;

            this.makeAceOptions();

            this.listenChanges()

        }


        protected makeAceOptions(){
            var that = this;
            this.aceOptions = {
                fontSize: Number(this.$source.get('fontSize')),
                theme: this.$source.get('theme'),
                mode:'html',
                showGutter:this.$source.get('showGutter'),
                rendererOptions:{
                    showInvisibles: this.$source.get('showInvisibles')
                },
                onLoad:function(_editor){
                    that._editor = _editor;
                    that._session = _editor.getSession();
                    that._renderer = that._editor.renderer;
                    that._renderer.setOption('fontSize' , Number(that.$source.get('fontSize')));
                    that._editor.setOption('highlightActiveLine' , that.$source.get('highlightActiveLine'));
                    that._session.setOption('wrap' , that.$source.get('wrap'));
                    that._session.setOption('tabSize' , Number(that.$source.get('tabSize')));
                    that._session.setOption('useSoftTabs' , that.$source.get('useSoftTabs') );
                    that._editor.$blockScrolling = Infinity;
                }

            }
        }

        protected listenChanges(){
            var that = this;
            this.$source.listen(function($key,$value){
                var newValue = $value;
                switch($key){
                    case 'fontSize':
                        that._renderer.setOption('fontSize' , Number(newValue));
                        break;
                    case 'showInvisibles':
                        that._renderer.setOption('showInvisibles' , newValue);
                        break;
                    case 'highlightActiveLine':
                        that._editor.setOption('highlightActiveLine' , newValue);
                        break;
                    case 'showGutter':
                        that._renderer.setOption('showGutter' , newValue);
                        break;
                    case 'wrap':
                        that._session.setOption('wrap' , newValue);
                        break;
                    case 'tabSize':
                        that._session.setOption('tabSize' , Number(newValue));
                        break;
                    case 'useSoftTabs':
                        that._session.setOption('useSoftTabs' , !newValue);
                        break;
                    case 'theme':
                        var gutter = that._renderer.getOption('showGutter');
                        that.aceOptions.theme = newValue;
                        that._renderer.setOption('showGutter' , gutter);
                        break;

                }
            });
        }
    }
}