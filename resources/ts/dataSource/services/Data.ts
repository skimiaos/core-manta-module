/// <reference path="../../framework/osService.ts"/>

module dataSource.services {
    export class $dataSource extends osml.framework.osService{

        public dataSourcesTypes:any = {
            kvp: {
                dataClass: 'ConfigDataSource'
            }
        };

        public dataModelsTypes:any = {
            ace: {
                dataClass: 'AceDataModel'
            }
        };

        public dataContexts:any = {};
        public dataModels:any = {};

        constructor(){
            super();
        }



        public source(type:string, context:string):types.DataSource{

            if(!angular.isDefined(this.dataContexts[type]))
                this.dataContexts[type] = {};

            if(angular.isDefined(this.dataContexts[type][context]))
                return this.dataContexts[type][context];
            else{
                var $source:types.DataSource = new dataSource.types[this.dataSourcesTypes[type].dataClass];
                this.dataContexts[type][context] = $source;

                return $source;
            }
        }

        public model(model:string, override_config:any){
            if(!angular.isDefined(this.dataModels[model]))
                this.dataModels[model] = [];

            var $model = new dataSource.models[this.dataModelsTypes[model].dataClass](this,override_config);


            this.dataModels[model].push($model);
            return $model;
        }

        public provideDefault(type:string, context:string,data:any){
            dataSource.types[this.dataSourcesTypes[type].dataClass].provideDefault(this.source(type,context),data);
        }


    }
}