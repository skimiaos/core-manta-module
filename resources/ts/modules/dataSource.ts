
/// <reference path="../vendors/angular.d.ts"/>
/// <reference path="../dataSource/services/Data.ts"/>


module dataSource {
    'use strict';

    export var app = angular.module('dataSource', []);

    for(var service in dataSource.services) {
        var name = service[0].toLowerCase() + service.slice(1);
        app.service(name, services[service]);
    }


    /*for(var directive in dataSource.directives){
        var name = directive[0].toLowerCase() + directive.slice(1);
        app.directive(name, directives[directive].Factory());
    };

    for(var controller in dataSource.controllers){
        app.controller(controller.valueOf(), controllers[controller]);
    };*/


}