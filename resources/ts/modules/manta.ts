/// <reference path="../vendors/framework.ts"/>

/// <reference path="../components/container/osContainer.ts"/>
/// <reference path="../components/container/osFlex.ts"/>

/// <reference path="../components/taskbar/osTaskbar.ts"/>
/// <reference path="../components/taskbar/osTaskbarItem.ts"/>
/// <reference path="../components/taskbar/osTaskbarUserpanel.ts"/>

/// <reference path="../components/dialogs/osTooltip.ts"/>

/// <reference path="../components/sidenav/osSidenav.ts"/>
/// <reference path="../components/sidenav/SidenavCtrl.ts"/>
/// <reference path="../components/sidenav/osSidenavItem.ts"/>
/// <reference path="../components/sidenav/menu/osSidenavMenu.ts"/>
/// <reference path="../components/sidenav/menu/osSidenavMenuItem.ts"/>
/// <reference path="../components/sidenav/browser/osSidenavBrowser.ts"/>
/// <reference path="../components/sidenav/browser/SidenavBrowserCtrl.ts"/>
/// <reference path="../components/sidenav/browser/osSidenavBrowserItem.ts"/>
/// <reference path="../components/sidenav/browser/osSidenavBrowserToolbox.ts"/>

/// <reference path="../components/forms/osInput.ts"/>
/// <reference path="../components/forms/osSelect.ts"/>

/// <reference path="../components/tabs/osTab.ts"/>
/// <reference path="../components/tabs/osTabContentTransclude.ts"/>
/// <reference path="../components/tabs/osTabHeadingTransclude.ts"/>
/// <reference path="../components/tabs/osTabset.ts"/>



declare var __osml_config: {
    base_path:string;
}

var MantaApp = new Skimia.Angular.Module('manta', []);
var app = MantaApp.module;

module osml {
    'use strict';

    /*for(var service in osml.services) {
        var name = service[0].toLowerCase() + service.slice(1);
        app.service(name, services[service]);
    }*/

    for(var directive in osml.directives){
        var name = directive[0].toLowerCase() + directive.slice(1);
        app.directive(name, directives[directive].Factory());
    };

    for(var controller in osml.controllers){
        app.controller(controller.valueOf(), controllers[controller]);
    };

    app.filter('toArray', function() {
    return function(items) {
        if(items){
            var keys = Object.keys(items);
            var arr = [];
            for (var i = 0; i < keys.length; i++) {
                arr.push(items[keys[i]]);
            }
            return arr;
        }
        else return [];
    };
});

}
