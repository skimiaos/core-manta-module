var osml;
(function (osml) {
    var framework;
    (function (framework) {
        'use strict';
        var osController = (function () {
            function osController($scope) {
                this.scope = $scope;
                this.scope['$events'] = this;
            }
            osController.$inject = ['$scope'];
            return osController;
        })();
        framework.osController = osController;
    })(framework = osml.framework || (osml.framework = {}));
})(osml || (osml = {}));

var osml;
(function (osml) {
    var framework;
    (function (framework) {
        'use strict';
        var osService = (function () {
            function osService() {
            }
            return osService;
        })();
        framework.osService = osService;
    })(framework = osml.framework || (osml.framework = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/osService.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var dataSource;
(function (dataSource) {
    var services;
    (function (services) {
        var $dataSource = (function (_super) {
            __extends($dataSource, _super);
            function $dataSource() {
                _super.call(this);
                this.dataSourcesTypes = {
                    kvp: {
                        dataClass: 'ConfigDataSource'
                    }
                };
                this.dataModelsTypes = {
                    ace: {
                        dataClass: 'AceDataModel'
                    }
                };
                this.dataContexts = {};
                this.dataModels = {};
            }
            $dataSource.prototype.source = function (type, context) {
                if (!angular.isDefined(this.dataContexts[type]))
                    this.dataContexts[type] = {};
                if (angular.isDefined(this.dataContexts[type][context]))
                    return this.dataContexts[type][context];
                else {
                    var $source = new dataSource.types[this.dataSourcesTypes[type].dataClass];
                    this.dataContexts[type][context] = $source;
                    return $source;
                }
            };
            $dataSource.prototype.model = function (model, override_config) {
                if (!angular.isDefined(this.dataModels[model]))
                    this.dataModels[model] = [];
                var $model = new dataSource.models[this.dataModelsTypes[model].dataClass](this, override_config);
                this.dataModels[model].push($model);
                return $model;
            };
            $dataSource.prototype.provideDefault = function (type, context, data) {
                dataSource.types[this.dataSourcesTypes[type].dataClass].provideDefault(this.source(type, context), data);
            };
            return $dataSource;
        })(osml.framework.osService);
        services.$dataSource = $dataSource;
    })(services = dataSource.services || (dataSource.services = {}));
})(dataSource || (dataSource = {}));

/// <reference path="../vendors/angular.d.ts"/>
/// <reference path="../dataSource/services/Data.ts"/>
var dataSource;
(function (dataSource) {
    'use strict';
    dataSource.app = angular.module('dataSource', []);
    for (var service in dataSource.services) {
        var name = service[0].toLowerCase() + service.slice(1);
        dataSource.app.service(name, dataSource.services[service]);
    }
})(dataSource || (dataSource = {}));

/// <reference path="angular.d.ts"/>
var Skimia;
(function (Skimia) {
    var Angular;
    (function (Angular) {
        var Module = (function () {
            function Module(name, requires, configFn) {
                if (requires === void 0) { requires = []; }
                if (configFn === void 0) { configFn = function () { }; }
                this.module = null;
                this.module = angular.module(name, requires, configFn);
            }
            Module.prototype.bindService = function (name, service) {
                service.$inject = service.injectables;
                var srv = service.prototype;
                this.module.service(name, srv);
            };
            Module.prototype.bindFilter = function () {
            };
            Module.prototype.bindComponent = function (component) {
                var config = new component();
                var directiveFactory = function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i - 0] = arguments[_i];
                    }
                    var isDef = angular.isDefined;
                    var directive = {};
                    if (isDef(config.restrict)) {
                        directive['restrict'] = config.restrict;
                    }
                    if (isDef(config.priority)) {
                        directive['priority'] = config.priority;
                    }
                    if (isDef(config.view)) {
                        if (isDef(config.view.templateCache)) {
                            var $templateCache = angular.element(document.body).injector().get('$templateCache');
                            directive['template'] = $templateCache.get(config.view.templateCache);
                        }
                        else if (isDef(config.view.templateUrl)) {
                            directive['templateUrl'] = config.view.templateUrl;
                        }
                        else if (isDef(config.view.template)) {
                            directive['template'] = config.view.template;
                        }
                        if (isDef(config.view.transclude)) {
                            directive['transclude'] = config.view.transclude;
                        }
                        if (isDef(config.view.replace)) {
                            directive['replace'] = config.view.replace;
                        }
                    }
                    if (isDef(config.manager)) {
                        if (config.manager.class.injectables.length > 0) {
                            directive['controller'] = config.manager.class.injectables.concat(config.manager.class);
                        }
                        else {
                            directive['controller'] = config.manager.class;
                        }
                        directive['controllerAs'] = config.manager.alias;
                        if (isDef(config.manager.isolated) && config.manager.isolated) {
                            directive['scope'] = {};
                        }
                        else {
                            directive['scope'] = true;
                        }
                        if (isDef(config.manager.bindings)) {
                            directive['bindToController'] = config.manager.bindings;
                        }
                    }
                    if (isDef(config.compile)) {
                        directive['compile'] = config.compile;
                    }
                    else if (isDef(config.link)) {
                        directive['link'] = config.link;
                    }
                    return directive;
                };
                this.module.directive(config.selector, directiveFactory);
            };
            return Module;
        })();
        Angular.Module = Module;
        var Service = (function () {
            function Service() {
                var _this = this;
                var injectables = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    injectables[_i - 0] = arguments[_i];
                }
                this.services = {};
                angular.forEach(injectables, function (injectable, index) {
                    _this.services[_this.constructor['injectables'][index]] = injectable;
                });
                this.init();
            }
            Service.prototype.init = function () {
            };
            return Service;
        })();
        Angular.Service = Service;
        var Manager = (function () {
            function Manager() {
                var _this = this;
                var injectables = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    injectables[_i - 0] = arguments[_i];
                }
                this.services = {};
                angular.forEach(injectables, function (injectable, index) {
                    _this.services[_this.constructor['injectables'][index]] = injectable;
                });
                this.init();
            }
            Manager.prototype.init = function () {
            };
            Manager.injectables = [];
            return Manager;
        })();
        Angular.Manager = Manager;
        var Component = (function () {
            function Component() {
            }
            return Component;
        })();
        Angular.Component = Component;
    })(Angular = Skimia.Angular || (Skimia.Angular = {}));
})(Skimia || (Skimia = {}));

var osml;
(function (osml) {
    var framework;
    (function (framework) {
        'use strict';
        var osDirective = (function () {
            function osDirective(templateCache) {
                this.link = this.link.bind(this);
                this.services = {
                    $templates: templateCache
                };
            }
            osDirective.injector = function () {
                return this.baseInject.concat(this.$inject);
            };
            osDirective.prototype.link = function ($scope, element, attributes) {
                var dependencies = [];
                for (var _i = 3; _i < arguments.length; _i++) {
                    dependencies[_i - 3] = arguments[_i];
                }
                this.$scope = $scope;
            };
            osDirective.Factory = function () {
                var _this = this;
                var directive = function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i - 0] = arguments[_i];
                    }
                    return _this.construct(_this.valueOf(), args);
                };
                directive.$inject = this.injector();
                return directive;
            };
            osDirective.construct = function (constructor, args) {
                function F() {
                    constructor.apply(this, args);
                }
                F.prototype = constructor.prototype;
                return new F();
            };
            osDirective.$inject = [];
            osDirective.baseInject = ['$templateCache'];
            return osDirective;
        })();
        framework.osDirective = osDirective;
    })(framework = osml.framework || (osml.framework = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var framework;
    (function (framework) {
        'use strict';
        var osJqueryDirective = (function (_super) {
            __extends(osJqueryDirective, _super);
            function osJqueryDirective(templates, $timeout) {
                _super.call(this, templates);
                this.services['$timeout'] = $timeout;
            }
            osJqueryDirective.injector = function () {
                var inject = this.baseInject.concat(this.jqueryInject);
                return inject.concat(this.$inject);
            };
            osJqueryDirective.jqueryInject = ['$timeout'];
            return osJqueryDirective;
        })(framework.osDirective);
        framework.osJqueryDirective = osJqueryDirective;
    })(framework = osml.framework || (osml.framework = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osContainer = (function (_super) {
            __extends(osContainer, _super);
            function osContainer(templates, $timeout) {
                _super.call(this, templates, $timeout);
                this.scope = {
                    direction: '@',
                    gutter: '@'
                };
            }
            osContainer.prototype.link = function ($scope, element, attributes) {
                _super.prototype.link.call(this, $scope, element, attributes);
                element.addClass('os-container');
                if (attributes.gutter)
                    element.children().css('margin-right', attributes.gutter);
                switch ($scope.direction) {
                    case 'row':
                        element.addClass('row');
                        break;
                    case 'row-reverse':
                        element.addClass('row-reverse');
                        break;
                    case 'column':
                        element.addClass('column');
                        break;
                    case 'column-reverse':
                        element.addClass('column-reverse');
                        break;
                    default:
                        console.error('os-container: invalid direction : ' + $scope.direction + ' mis par defaut en row');
                        element.addClass('row');
                        break;
                }
            };
            return osContainer;
        })(osml.framework.osJqueryDirective);
        directives.osContainer = osContainer;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osFlex = (function (_super) {
            __extends(osFlex, _super);
            function osFlex() {
                _super.apply(this, arguments);
                this.restrict = 'A';
            }
            osFlex.prototype.link = function ($scope, element, attributes) {
                _super.prototype.link.call(this, $scope, element, attributes);
                this.services.$timeout(function () {
                    $(element).css('flex', attributes.osFlex);
                }, 0);
            };
            return osFlex;
        })(osml.framework.osJqueryDirective);
        directives.osFlex = osFlex;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osTaskbar = (function (_super) {
            __extends(osTaskbar, _super);
            function osTaskbar(templates, $timeout) {
                _super.call(this, templates, $timeout);
                this.template = this.services.$templates.get('osTaskbar.html');
                this.transclude = true;
                this.scope = {};
            }
            osTaskbar.prototype.link = function ($scope, element, attributes) {
                _super.prototype.link.call(this, $scope, element, attributes);
                this.services.$timeout(function () {
                    $(element).addClass('os-taskbar');
                }, 0);
            };
            return osTaskbar;
        })(osml.framework.osJqueryDirective);
        directives.osTaskbar = osTaskbar;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var framework;
    (function (framework) {
        'use strict';
        var osDataloadDirective = (function (_super) {
            __extends(osDataloadDirective, _super);
            function osDataloadDirective(templates, $q) {
                _super.call(this, templates);
                this.services['$q'] = $q;
            }
            osDataloadDirective.injector = function () {
                var inject = this.baseInject.concat(this.dataloadInject);
                return inject.concat(this.$inject);
            };
            osDataloadDirective.prototype.loadData = function (scopeAttr) {
                var loading = this.services.$q.defer();
                if (this.$scope[scopeAttr]) {
                    loading.resolve(this.$scope[scopeAttr]);
                }
                else {
                    this.$scope.$watch(scopeAttr, function (newValue, oldValue) {
                        if (newValue !== undefined)
                            loading.resolve(newValue);
                    });
                }
                return loading.promise;
            };
            osDataloadDirective.dataloadInject = ['$q'];
            return osDataloadDirective;
        })(framework.osDirective);
        framework.osDataloadDirective = osDataloadDirective;
    })(framework = osml.framework || (osml.framework = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osDataloadDirective.ts"/>
/// <reference path="../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var framework;
    (function (framework) {
        'use strict';
        var osJqueryDataloadDirective = (function (_super) {
            __extends(osJqueryDataloadDirective, _super);
            function osJqueryDataloadDirective(templates, $q, $timeout) {
                _super.call(this, templates, $q);
                this.services['$timeout'] = $timeout;
            }
            osJqueryDataloadDirective.injector = function () {
                var inject = this.baseInject.concat(this.dataloadInject);
                inject = inject.concat(this.jqueryInject);
                return inject.concat(this.$inject);
            };
            osJqueryDataloadDirective.jqueryInject = ['$timeout'];
            return osJqueryDataloadDirective;
        })(framework.osDataloadDirective);
        framework.osJqueryDataloadDirective = osJqueryDataloadDirective;
    })(framework = osml.framework || (osml.framework = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osJqueryDataloadDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osTaskbarItem = (function (_super) {
            __extends(osTaskbarItem, _super);
            function osTaskbarItem() {
                _super.apply(this, arguments);
                this.template = this.services.$templates.get('osTaskbarItem.html');
                this.transclude = true;
                this.scope = {
                    color: '@',
                    href: '@',
                    icon: '@',
                    active: '=',
                    close: '&'
                };
            }
            osTaskbarItem.prototype.link = function ($scope, element, attrs) {
                var _this = this;
                _super.prototype.link.call(this, $scope, element, attrs);
                this.services.$q.all([
                    this.loadData('active')
                ]).then(function () {
                    _this.services.$timeout(function () {
                        $scope.$watch('active', function (value) {
                            if (value)
                                $(element).find('li').addClass($scope.color);
                            else
                                $(element).find('li').removeClass($scope.color);
                        });
                    }, 0);
                });
                this.services.$timeout(function () {
                    $(element).addClass('os-taskbar-item');
                    $(element).find('li').on('mouseenter', function () {
                        $(element).find('li').addClass($scope.color);
                        $(element).find('p').addClass('active');
                        $(element).find('.os-taskbar-item--close').css('visibility', 'visible');
                    });
                    $(element).find('li').on('mouseleave', function () {
                        if (!$scope.active)
                            $(element).find('li').removeClass($scope.color);
                        $(element).find('p').removeClass('active');
                        $(element).find('.os-taskbar-item--close').css('visibility', 'hidden');
                    });
                    $(element).find('.os-taskbar-item--close').on('mouseenter', function () {
                        $(element).find('.os-taskbar-item--close').removeClass('mdi-navigation-close');
                        $(element).find('.os-taskbar-item--close').addClass('mdi-navigation-cancel');
                    });
                    $(element).find('.os-taskbar-item--close').on('mouseout', function () {
                        $(element).find('.os-taskbar-item--close').removeClass('mdi-navigation-cancel');
                        $(element).find('.os-taskbar-item--close').addClass('mdi-navigation-close');
                    });
                }, 0);
            };
            return osTaskbarItem;
        })(osml.framework.osJqueryDataloadDirective);
        directives.osTaskbarItem = osTaskbarItem;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osTaskbarUserpanel = (function (_super) {
            __extends(osTaskbarUserpanel, _super);
            function osTaskbarUserpanel(templates, $timeout) {
                _super.call(this, templates, $timeout);
                this.template = this.services.$templates.get('osTaskbarUserpanel.html');
                this.transclude = true;
                this.scope = {
                    color: '@',
                    username: '='
                };
            }
            osTaskbarUserpanel.prototype.link = function ($scope, element, attributes) {
                _super.prototype.link.call(this, $scope, element, attributes);
                this.services.$timeout(function () {
                    $(element).addClass('os-taskbar-userpanel');
                    $(element).find('.os-taskbar-userpanel--btn').on('click', function () {
                        if ($(element).find('.os-taskbar-userpanel--menu').hasClass('is-open'))
                            $(element).find('.os-taskbar-userpanel--menu').removeClass('is-open');
                        else
                            $(element).find('.os-taskbar-userpanel--menu').addClass('is-open');
                    });
                }, 0);
            };
            return osTaskbarUserpanel;
        })(osml.framework.osJqueryDirective);
        directives.osTaskbarUserpanel = osTaskbarUserpanel;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osToolbox = (function (_super) {
            __extends(osToolbox, _super);
            function osToolbox(templates, $timeout) {
                _super.call(this, templates, $timeout);
                this.scope = {};
            }
            osToolbox.prototype.link = function ($scope, element, attributes) {
                _super.prototype.link.call(this, $scope, element, attributes);
                this.services.$timeout(function () {
                    $('*[os-toolbox]').tooltip();
                }, 100);
                $scope.$on("$destroy", function handleDestroyEvent() {
                    $('.material-tooltip').remove();
                    $('*[os-toolbox]').tooltip();
                });
            };
            return osToolbox;
        })(osml.framework.osJqueryDirective);
        directives.osToolbox = osToolbox;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osSidenav = (function (_super) {
            __extends(osSidenav, _super);
            function osSidenav(templates, $timeout) {
                _super.call(this, templates, $timeout);
                this.template = this.services.$templates.get('osSidenav.html');
                this.transclude = true;
                this.controller = osml.controllers.SidenavController;
                this.scope = {
                    color: '@',
                    label: '@'
                };
            }
            osSidenav.prototype.link = function ($scope, element, attrs) {
                _super.prototype.link.call(this, $scope, element, attrs);
                this.services.$timeout(function () {
                    $(element).addClass('os-sidenav');
                }, 0);
            };
            return osSidenav;
        })(osml.framework.osJqueryDirective);
        directives.osSidenav = osSidenav;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

var osml;
(function (osml) {
    var controllers;
    (function (controllers) {
        var SidenavController = (function () {
            function SidenavController($scope, $element) {
                this.$element = $element;
                this.$scope = $scope;
                this.$scope.hasMenu = false;
                this.$scope.$watch('hasMenu', function (value) {
                    if (value)
                        $($element).addClass('hasMenu');
                    else
                        $($element).removeClass('hasMenu');
                });
            }
            SidenavController.prototype.openMenu = function (isOpen) {
                this.$scope.hasMenu = isOpen;
                this.$scope.$parent.$broadcast('sidenavopenchanged', isOpen);
                if (this.$scope.hasMenu)
                    $(this.$element).addClass('hasMenu');
                else
                    $(this.$element).removeClass('hasMenu');
            };
            SidenavController.prototype.getColor = function () {
                return this.$scope.color;
            };
            SidenavController.prototype.removeActive = function () {
                $(this.$element).find('.os-sidenav-item.active').removeClass('active');
                $(this.$element).find('.os-sidenav-menu.isOpen').removeClass('isOpen');
                $(this.$element).find('.os-sidenav-browser.isOpen').removeClass('isOpen');
                $(this.$element).find('.os-sidenav-item--link p').removeClass('hidden');
                this.openMenu(false);
            };
            return SidenavController;
        })();
        controllers.SidenavController = SidenavController;
    })(controllers = osml.controllers || (osml.controllers = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osSidenavItem = (function (_super) {
            __extends(osSidenavItem, _super);
            function osSidenavItem() {
                _super.apply(this, arguments);
                this.template = this.services.$templates.get('osSidenavItem.html');
                this.transclude = true;
                this.require = '^osSidenav';
                this.scope = {
                    href: '@',
                    icon: '@',
                    color: '@?',
                    label: '@',
                    active: '='
                };
            }
            osSidenavItem.prototype.link = function ($scope, element, attrs, sidenavCtrl) {
                _super.prototype.link.call(this, $scope, element, attrs);
                this.registerEvents(element, sidenavCtrl);
                this.services.$timeout(function () {
                    $(element).addClass('os-sidenav-item');
                    if ($scope.active) {
                        $(element).find('.os-sidenav-item--link').trigger('click');
                    }
                    if ($scope.href)
                        $(element).children('li').find('a').attr('href', $scope.href);
                    $scope.color = angular.isDefined($scope.color) ? $scope.color : sidenavCtrl.getColor();
                }, 0);
            };
            osSidenavItem.prototype.registerEvents = function (element, sidenavCtrl) {
                $(element).find('.os-sidenav-item--link').on('mouseenter', function () {
                    $(element).addClass('hover');
                });
                $(element).find('.os-sidenav-item--link').on('mouseleave', function () {
                    $(element).removeClass('hover');
                });
                $(element).find('.os-sidenav-item--link').on('click', function () {
                    var submenu = $(element).find('.os-sidenav-item--submenu').children();
                    var hClass = submenu.hasClass('isOpen');
                    if (submenu.length || submenu.hasClass('isOpen')) {
                        sidenavCtrl.openMenu(false);
                        $(element).find('.os-sidenav-item--link p').removeClass('hidden');
                        if (submenu.length)
                            submenu.removeClass('isOpen');
                        sidenavCtrl.removeActive();
                    }
                    if (!submenu.hasClass('isOpen') && submenu.length && !hClass) {
                        $(element).removeClass('hover');
                        sidenavCtrl.openMenu(true);
                        $(element).find('.os-sidenav-item--link p').addClass('hidden');
                        submenu.addClass('isOpen');
                        $(element).addClass('active');
                    }
                    else if (!submenu.length || submenu.hasClass('isOpen')) {
                        sidenavCtrl.openMenu(false);
                        $(element).find('.os-sidenav-item--link p').removeClass('hidden');
                        if (submenu.length)
                            submenu.removeClass('isOpen');
                        sidenavCtrl.removeActive();
                    }
                });
            };
            return osSidenavItem;
        })(osml.framework.osJqueryDirective);
        directives.osSidenavItem = osSidenavItem;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osSidenavMenu = (function (_super) {
            __extends(osSidenavMenu, _super);
            function osSidenavMenu() {
                _super.apply(this, arguments);
                this.template = this.services.$templates.get('osSidenavMenu.html');
                this.transclude = true;
                this.controller = osml.controllers.SidenavMenuController;
                this.scope = {
                    title: '@'
                };
            }
            osSidenavMenu.prototype.link = function ($scope, element, attrs) {
                _super.prototype.link.call(this, $scope, element, attrs);
                this.services.$timeout(function () {
                    $(element).addClass('os-sidenav-menu');
                    $scope.title = $(element).parents('os-sidenav-item').children('li').find('p').text();
                }, 0);
            };
            return osSidenavMenu;
        })(osml.framework.osJqueryDataloadDirective);
        directives.osSidenavMenu = osSidenavMenu;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osSidenavMenuItem = (function (_super) {
            __extends(osSidenavMenuItem, _super);
            function osSidenavMenuItem(templates, $timeout) {
                _super.call(this, templates, $timeout);
                this.template = this.services.$templates.get('osSidenavMenuItem.html');
                this.transclude = true;
                this.require = '^osSidenavMenu';
                this.scope = {
                    name: '@',
                    icon: '@'
                };
            }
            osSidenavMenuItem.prototype.link = function ($scope, element, attrs) {
                _super.prototype.link.call(this, $scope, element, attrs);
                if ($scope.active == undefined)
                    $scope.active = false;
                this.services.$timeout(function () {
                    $(element).addClass('os-sidenav-menu-item');
                }, 0);
            };
            osSidenavMenuItem.prototype.registerEvents = function (element, sidenavMenuCtrl) {
                $(element).on('click', function () {
                    if (!$(element).hasClass('active')) {
                        sidenavMenuCtrl.removeActive();
                        $(element).addClass('active');
                    }
                });
            };
            return osSidenavMenuItem;
        })(osml.framework.osJqueryDirective);
        directives.osSidenavMenuItem = osSidenavMenuItem;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osSidenavBrowser = (function (_super) {
            __extends(osSidenavBrowser, _super);
            function osSidenavBrowser($template, $q, $timeout) {
                _super.call(this, $template, $q, $timeout);
                this.template = this.services.$templates.get('osSidenavBrowser.html');
                this.scope = {
                    model: '=ngModel',
                    events: '=?'
                };
                this.isFile = this.isFile.bind(this);
            }
            osSidenavBrowser.prototype.link = function ($scope, element, attrs) {
                var _this = this;
                _super.prototype.link.call(this, $scope, element, attrs);
                var load = function ($auto_bread) {
                    $scope.current_directory = _this.adaptModel($scope.model);
                    $scope.history = [];
                    $scope.breadcrumb = [];
                    $scope.directory = 'racine';
                    _this.services.$timeout(function () {
                        $(element).addClass('os-sidenav-browser');
                        $scope.title = $(element).parents('os-sidenav-item').children('li').find('p').text();
                    }, 0);
                    $scope.addFile = function () {
                        if ($scope.breadcrumb.length == 0) {
                            return $scope.events.create('');
                        }
                        else {
                            var crumb = [$scope.directory] + $scope.breadcrumb.slice(1);
                            return $scope.events.create(crumb.concat('/'));
                        }
                    };
                    $scope.openFile = function (name) {
                        var file = $scope.current_directory[name];
                        if (file == false)
                            return;
                        if (_this.isFile(file)) {
                            $scope.events.open(file);
                        }
                        else {
                            _this.changeDirectory($scope, name);
                        }
                    };
                    $scope.parent = function () {
                        _this.parentDirectory($scope);
                    };
                    angular.forEach($auto_bread, function (file) {
                        $scope.openFile(file);
                    });
                };
                this.services.$q.all([
                    this.loadData('model')
                ]).then(load([]));
                $scope.$watch('model', function (newValue, oldValue) {
                    if ($scope.breadcrumb.length == 0)
                        load([]);
                    else {
                        load(angular.extend([$scope.directory], $scope.breadcrumb.slice(1)));
                    }
                });
            };
            osSidenavBrowser.prototype.isFile = function (file) {
                if (!file)
                    return false;
                if (angular.isDefined(file.__TYPE) && file.__TYPE == 'FILE')
                    return true;
                return angular.isDefined(file.mime) && typeof file['mime'] == 'string';
            };
            osSidenavBrowser.prototype.adaptModel = function (model) {
                var _this = this;
                angular.forEach(model, function (file, name) {
                    if (_this.isFile(file)) {
                        file['isDirectory'] = false;
                        file['icon'] = _this.iconizeMimeType(file['mime']);
                        if (!angular.isDefined(file['info']))
                            file['info'] = file['size'] + ', Modifié le ' + file['modified'];
                        file['_name'] = name;
                    }
                    else {
                        if (!angular.isDefined(file['childrens'])) {
                            var f = angular.copy(file);
                            file = {
                                childrens: f
                            };
                        }
                        file['isDirectory'] = true;
                        file['icon'] = 'mdi-file-folder';
                        file['directories'] = 0;
                        file['files'] = 0;
                        angular.forEach(file['childrens'], function (sub_file, name) {
                            if (_this.isFile(sub_file))
                                file['files']++;
                            else
                                file['directories']++;
                        });
                        file['info'] = '';
                        if (file['directories'] > 0)
                            file['info'] = file['directories'] + ' répertoire' + (file['directories'] > 1 ? 's' : '');
                        if (file['files'] > 0)
                            file['info'] += (file['directories'] > 0 ? ', ' : '') + file['files'] + ' fichier' + (file['files'] > 1 ? 's' : '');
                        file['_name'] = name;
                    }
                    model[name] = file;
                });
                return model;
            };
            osSidenavBrowser.prototype.iconizeMimeType = function (mime) {
                var mimes = mime.split('/');
                switch (mimes[0]) {
                    case 'image':
                        return 'mdi-image-crop-original';
                        break;
                    case 'text':
                        return 'mdi-editor-format-align-left';
                        break;
                }
                return 'os-icon-help-1';
            };
            osSidenavBrowser.prototype.openFile = function (name) {
            };
            osSidenavBrowser.prototype.changeDirectory = function ($scope, name) {
                $scope.history.push($scope.current_directory);
                $scope.breadcrumb.push($scope.directory);
                $scope.current_directory = this.adaptModel($scope.current_directory[name]['childrens']);
                $scope.directory = name;
            };
            osSidenavBrowser.prototype.parentDirectory = function ($scope) {
                $scope.current_directory = $scope.history.pop();
                $scope.directory = $scope.breadcrumb.pop();
            };
            return osSidenavBrowser;
        })(osml.framework.osJqueryDataloadDirective);
        directives.osSidenavBrowser = osSidenavBrowser;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

var osml;
(function (osml) {
    var controllers;
    (function (controllers) {
        var SidenavBrowserController = (function () {
            function SidenavBrowserController($scope, $element) {
                this.$element = $element;
                this.$scope = $scope;
            }
            SidenavBrowserController.prototype.search = function (query) {
                this.$scope.searchQuery = query;
            };
            SidenavBrowserController.prototype.clearSearch = function () {
                this.$scope.searchQuery = '';
                console.log('fu');
            };
            return SidenavBrowserController;
        })();
        controllers.SidenavBrowserController = SidenavBrowserController;
    })(controllers = osml.controllers || (osml.controllers = {}));
})(osml || (osml = {}));

/// <reference path="../../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osSidenavBrowserItem = (function (_super) {
            __extends(osSidenavBrowserItem, _super);
            function osSidenavBrowserItem(templates, $timeout) {
                _super.call(this, templates, $timeout);
                this.template = this.services.$templates.get('osSidenavBrowserItem.html');
                this.transclude = true;
                this.scope = {
                    name: '@',
                    icon: '@'
                };
            }
            osSidenavBrowserItem.prototype.link = function ($scope, element, attrs) {
                _super.prototype.link.call(this, $scope, element, attrs);
                this.services.$timeout(function () {
                    $(element).addClass('os-sidenav-browser-item');
                }, 0);
            };
            return osSidenavBrowserItem;
        })(osml.framework.osJqueryDirective);
        directives.osSidenavBrowserItem = osSidenavBrowserItem;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../../framework/directives/osJqueryDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osSidenavBrowserToolbox = (function (_super) {
            __extends(osSidenavBrowserToolbox, _super);
            function osSidenavBrowserToolbox(templates, $timeout) {
                _super.call(this, templates, $timeout);
                this.template = this.services.$templates.get('osSidenavBrowserToolbox.html');
                this.transclude = true;
                this.scope = {
                    name: '@',
                    icon: '@',
                    returnClick: '&',
                    search: '&',
                    showReturn: '=',
                    add: '&'
                };
            }
            osSidenavBrowserToolbox.prototype.link = function ($scope, element, attrs) {
                _super.prototype.link.call(this, $scope, element, attrs);
                $scope.query = { text: '' };
                this.services.$timeout(function () {
                    $(element).addClass('os-sidenav-browser-toolbox');
                    $(element).find('.os-sidenav-browser-toolbox--search input').on('focus', function () {
                        $(element).find('.os-sidenav-browser-toolbox--search').addClass('isFocus');
                    });
                    $(element).find('.os-sidenav-browser-toolbox--search input').on('focusout', function () {
                        $(element).find('.os-sidenav-browser-toolbox--search').removeClass('isFocus');
                    });
                    $(element).find('.os-sidenav-browser-toolbox--search input').on('keyup', function () {
                        if ($(element).find('.os-sidenav-browser-toolbox--search input').val() != '') {
                            $(element).find('.os-sidenav-browser-toolbox--search').addClass('hasText');
                        }
                        else {
                            $(element).find('.os-sidenav-browser-toolbox--search').removeClass('hasText');
                        }
                    });
                    $(element).find('.os-sidenav-browser-toolbox--search form > i').on('click', function () {
                        $(element).find('.os-sidenav-browser-toolbox--search').removeClass('isFocus');
                        $(element).find('.os-sidenav-browser-toolbox--search').removeClass('hasText');
                        $(element).find('.os-sidenav-browser-toolbox--search input').val('');
                    });
                }, 0);
            };
            return osSidenavBrowserToolbox;
        })(osml.framework.osJqueryDirective);
        directives.osSidenavBrowserToolbox = osSidenavBrowserToolbox;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osJqueryDataloadDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osInput = (function (_super) {
            __extends(osInput, _super);
            function osInput(templates, $q, $timeout) {
                _super.call(this, templates, $q, $timeout);
                this.template = this.services.$templates.get('osInput.html');
                this.transclude = true;
                this.scope = {
                    name: '@',
                    type: '@',
                    placeholder: '@',
                    attributes: '@',
                    model: '=ngModel',
                    disabled: '=osDisabled'
                };
            }
            osInput.prototype.link = function ($scope, element, attributes) {
                var _this = this;
                _super.prototype.link.call(this, $scope, element, attributes);
                if (!attributes.type)
                    $scope.type = 'text';
                $(element).addClass('os-input col s12');
                this.services.$q.all([
                    this.loadData('model')
                ]).then(function () {
                    _this.services.$timeout(function () {
                        if ($scope.disabled == undefined)
                            $scope.disabled = false;
                        if ($scope.placeholder == undefined)
                            $(element).children('input').removeAttr('placeholder');
                        else
                            $(element).children('label').addClass('active');
                        if ($scope.model == undefined)
                            $(element).children('input').removeAttr('ng-model');
                        if ($scope.model != '') {
                            $(element).children('label').addClass('active');
                        }
                    }, 1);
                });
            };
            return osInput;
        })(osml.framework.osJqueryDataloadDirective);
        directives.osInput = osInput;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osJqueryDataloadDirective.ts"/>
/// <reference path="../../vendors/materializecss.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osSelect = (function (_super) {
            __extends(osSelect, _super);
            function osSelect(templates, $q, $timeout) {
                _super.call(this, templates, $q, $timeout);
                this.template = this.services.$templates.get('osSelect.html');
                this.transclude = true;
                this.scope = {
                    name: '@',
                    options: '=',
                    model: '=ngModel',
                    placeholder: '@'
                };
            }
            osSelect.prototype.link = function ($scope, element, attributes) {
                var _this = this;
                _super.prototype.link.call(this, $scope, element, attributes);
                $scope.ng = angular;
                this.services.$q.all([
                    this.loadData('model'),
                    this.loadData('options'),
                    this.loadData('placeholder')
                ]).then(function () {
                    if ($scope.model == null)
                        $scope.model = '__placeholder';
                    $scope.model = $scope.model.toString();
                    console.log($scope);
                    console.log($scope.ng.extend({ '__placeholder': $scope.placeholder }, $scope.options));
                    _this.services.$timeout(function () {
                        $(element).addClass('os-select col s12');
                        $(element).children('select').material_select();
                    }, 0);
                });
            };
            return osSelect;
        })(osml.framework.osJqueryDataloadDirective);
        directives.osSelect = osSelect;
        ;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osTab = (function (_super) {
            __extends(osTab, _super);
            function osTab(templates, $q, $timer, $parse, $log) {
                _super.call(this, templates, $q, $timer);
                this.require = '^osTabset';
                this.restrict = 'EA';
                this.replace = true;
                this.transclude = true;
                this.template = this.services.$templates.get('osTab.html');
                this.scope = {
                    active: '=?',
                    heading: '@',
                    onSelect: '&select',
                    onDeselect: '&deselect',
                    close: '&',
                    modified: '=?'
                };
                this.services['$parse'] = $parse;
                this.services['$log'] = $log;
            }
            osTab.prototype.controller = function () { };
            osTab.prototype.compile = function (elms, attrs, transclude) {
                var that = this;
                return function postLink(scope, elm, attrs, osTabsetCtrl) {
                    if (!angular.isDefined(scope.modified)) {
                        scope.modified = false;
                    }
                    scope.$watch('active', function (active) {
                        if (active) {
                            that.services.$timeout(function () { elm.find('a').trigger('click'); });
                            osTabsetCtrl.select(scope, elm);
                        }
                    });
                    scope.disabled = false;
                    if (attrs.disable) {
                        scope.$parent.$watch(this.services.$parse(attrs.disable), function (value) {
                            scope.disabled = !!value;
                        });
                    }
                    if (attrs.disabled) {
                        this.services.$log.warn('Use of "disabled" attribute has been deprecated, please use "disable"');
                        scope.$parent.$watch(this.services.$parse(attrs.disabled), function (value) {
                            scope.disabled = !!value;
                        });
                    }
                    scope.select = function () {
                        if (!scope.disabled) {
                            scope.active = true;
                        }
                    };
                    osTabsetCtrl.addTab(scope);
                    scope.$on('$destroy', function () {
                        osTabsetCtrl.removeTab(scope);
                    });
                    scope.$transcludeFn = transclude;
                    scope.$element = elms;
                    that.services.$timeout(function () {
                        var width = $(elm).find('.tab').width() + 72;
                        $(elm).css('width', width + 'px');
                        $(elm).find('.tab i').on('mouseenter', function () {
                            $(elm).find('.tab i').removeClass('mdi-alert-error').removeClass('mdi-navigation-close');
                            $(elm).find('.tab i').addClass('mdi-navigation-cancel');
                        });
                        $(elm).find('.tab i').on('mouseout', function () {
                            $(elm).find('.tab i').removeClass('mdi-navigation-cancel');
                            if (scope.modified)
                                $(elm).find('.tab i').addClass('mdi-alert-error');
                            else
                                $(elm).find('.tab i').addClass('mdi-navigation-close');
                        });
                    }, 0);
                };
            };
            osTab.prototype.link = function ($scope, element, attributes) {
                _super.prototype.link.call(this, $scope, element, attributes);
            };
            osTab.$inject = ['$q', '$timeout', '$parse', '$log'];
            return osTab;
        })(osml.framework.osJqueryDataloadDirective);
        directives.osTab = osTab;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osTabContentTransclude = (function (_super) {
            __extends(osTabContentTransclude, _super);
            function osTabContentTransclude(templates, $q, $timer) {
                _super.call(this, templates, $q, $timer);
                this.require = '^?osTab';
                this.restrict = 'A';
                this.scope = true;
            }
            osTabContentTransclude.prototype.link = function ($scope, element, attributes) {
                _super.prototype.link.call(this, $scope, element, attributes);
                var tab = $scope.$parent.$eval(attributes.osTabContentTransclude);
                var that = this;
                tab.$transcludeFn(tab.$parent, function (contents) {
                    angular.forEach(contents, function (node) {
                        if (that.isTabHeading(node)) {
                            tab.headingElement = node;
                        }
                        else {
                            element.append(node);
                        }
                    });
                });
            };
            osTabContentTransclude.prototype.isTabHeading = function (node) {
                return node.tagName && (node.hasAttribute('tab-heading') ||
                    node.hasAttribute('data-tab-heading') ||
                    node.tagName.toLowerCase() === 'tab-heading' ||
                    node.tagName.toLowerCase() === 'data-tab-heading');
            };
            return osTabContentTransclude;
        })(osml.framework.osJqueryDataloadDirective);
        directives.osTabContentTransclude = osTabContentTransclude;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osTabHeadingTransclude = (function (_super) {
            __extends(osTabHeadingTransclude, _super);
            function osTabHeadingTransclude(templates, $q, $timer) {
                _super.call(this, templates, $q, $timer);
                this.require = '^osTab';
                this.restrict = 'A';
            }
            osTabHeadingTransclude.prototype.link = function ($scope, element, attributes) {
                _super.prototype.link.call(this, $scope, element, attributes);
                $scope.$watch('headingElement', function updateHeadingElement(heading) {
                    if (heading) {
                        element.html('');
                        element.append(heading);
                    }
                });
            };
            osTabHeadingTransclude.$inject = ['$q', '$timeout'];
            return osTabHeadingTransclude;
        })(osml.framework.osJqueryDataloadDirective);
        directives.osTabHeadingTransclude = osTabHeadingTransclude;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));

/// <reference path="../../framework/directives/osDirective.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var osml;
(function (osml) {
    var directives;
    (function (directives) {
        'use strict';
        var osTabset = (function (_super) {
            __extends(osTabset, _super);
            function osTabset(templates, $q, $timer) {
                _super.call(this, templates, $q, $timer);
                this.restrict = 'EA';
                this.transclude = true;
                this.template = this.services.$templates.get('osTabset.html');
                this.scope = {
                    type: '@'
                };
                this.controller = 'TabsetController';
            }
            osTabset.prototype.link = function ($scope, element, attributes) {
                _super.prototype.link.call(this, $scope, element, attributes);
                $(element).addClass('os-tabset');
                $scope.vertical = angular.isDefined(attributes.vertical) ? attributes.$parent.$eval(attributes.vertical) : false;
                $scope.justified = angular.isDefined(attributes.justified) ? attributes.$parent.$eval(attributes.justified) : false;
            };
            return osTabset;
        })(osml.framework.osJqueryDataloadDirective);
        directives.osTabset = osTabset;
    })(directives = osml.directives || (osml.directives = {}));
})(osml || (osml = {}));
var osml;
(function (osml) {
    var controllers;
    (function (controllers) {
        var TabsetController = (function () {
            function TabsetController($scope) {
                this.$scope = $scope;
                this.destroyed = false;
                this.tabs = $scope.tabs = [];
                $scope.$on('$destroy', function () {
                    this.destroyed = true;
                });
            }
            TabsetController.prototype.select = function (selectedTab) {
                angular.forEach(this.tabs, function (tab) {
                    if (tab.active && tab !== selectedTab) {
                        tab.active = false;
                        tab.onDeselect();
                    }
                });
                selectedTab.active = true;
                selectedTab.onSelect();
            };
            TabsetController.prototype.addTab = function (tab) {
                this.tabs.push(tab);
                if (this.tabs.length === 1 && tab.active !== false) {
                    tab.active = true;
                }
                else if (tab.active) {
                    this.select(tab);
                }
                else {
                    tab.active = false;
                }
                $('ul.tabs').tabs();
            };
            TabsetController.prototype.removeTab = function (tab) {
                var index = this.tabs.indexOf(tab);
                if (tab.active && this.tabs.length > 1 && !this.destroyed) {
                    var newActiveIndex = index == this.tabs.length - 1 ? index - 1 : index + 1;
                    this.select(this.tabs[newActiveIndex]);
                }
                this.tabs.splice(index, 1);
                $('ul.tabs').tabs();
            };
            TabsetController.$inject = ["$scope"];
            return TabsetController;
        })();
        controllers.TabsetController = TabsetController;
    })(controllers = osml.controllers || (osml.controllers = {}));
})(osml || (osml = {}));

/// <reference path="../vendors/framework.ts"/>
/// <reference path="../components/container/osContainer.ts"/>
/// <reference path="../components/container/osFlex.ts"/>
/// <reference path="../components/taskbar/osTaskbar.ts"/>
/// <reference path="../components/taskbar/osTaskbarItem.ts"/>
/// <reference path="../components/taskbar/osTaskbarUserpanel.ts"/>
/// <reference path="../components/dialogs/osTooltip.ts"/>
/// <reference path="../components/sidenav/osSidenav.ts"/>
/// <reference path="../components/sidenav/SidenavCtrl.ts"/>
/// <reference path="../components/sidenav/osSidenavItem.ts"/>
/// <reference path="../components/sidenav/menu/osSidenavMenu.ts"/>
/// <reference path="../components/sidenav/menu/osSidenavMenuItem.ts"/>
/// <reference path="../components/sidenav/browser/osSidenavBrowser.ts"/>
/// <reference path="../components/sidenav/browser/SidenavBrowserCtrl.ts"/>
/// <reference path="../components/sidenav/browser/osSidenavBrowserItem.ts"/>
/// <reference path="../components/sidenav/browser/osSidenavBrowserToolbox.ts"/>
/// <reference path="../components/forms/osInput.ts"/>
/// <reference path="../components/forms/osSelect.ts"/>
/// <reference path="../components/tabs/osTab.ts"/>
/// <reference path="../components/tabs/osTabContentTransclude.ts"/>
/// <reference path="../components/tabs/osTabHeadingTransclude.ts"/>
/// <reference path="../components/tabs/osTabset.ts"/>
var MantaApp = new Skimia.Angular.Module('manta', []);
var app = MantaApp.module;
var osml;
(function (osml) {
    'use strict';
    for (var directive in osml.directives) {
        var name = directive[0].toLowerCase() + directive.slice(1);
        app.directive(name, osml.directives[directive].Factory());
    }
    ;
    for (var controller in osml.controllers) {
        app.controller(controller.valueOf(), osml.controllers[controller]);
    }
    ;
    app.filter('toArray', function () {
        return function (items) {
            if (items) {
                var keys = Object.keys(items);
                var arr = [];
                for (var i = 0; i < keys.length; i++) {
                    arr.push(items[keys[i]]);
                }
                return arr;
            }
            else
                return [];
        };
    });
})(osml || (osml = {}));

var dataSource;
(function (dataSource) {
    var models;
    (function (models) {
        var KVPDataModel = (function () {
            function KVPDataModel($dataSource) {
                this.$dataService = $dataSource;
            }
            return KVPDataModel;
        })();
        models.KVPDataModel = KVPDataModel;
    })(models = dataSource.models || (dataSource.models = {}));
})(dataSource || (dataSource = {}));

/// <reference path="KVPDataModel.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var dataSource;
(function (dataSource) {
    var models;
    (function (models) {
        var AceDataModel = (function (_super) {
            __extends(AceDataModel, _super);
            function AceDataModel($dataSource, $config) {
                _super.call(this, $dataSource);
                this.$sourceContext = 'ace-editor';
                this.$source = $dataSource.source('kvp', this.$sourceContext);
                this.$config = $config;
                this.makeAceOptions();
                this.listenChanges();
            }
            AceDataModel.prototype.makeAceOptions = function () {
                var that = this;
                this.aceOptions = {
                    fontSize: Number(this.$source.get('fontSize')),
                    theme: this.$source.get('theme'),
                    mode: 'html',
                    showGutter: this.$source.get('showGutter'),
                    rendererOptions: {
                        showInvisibles: this.$source.get('showInvisibles')
                    },
                    onLoad: function (_editor) {
                        that._editor = _editor;
                        that._session = _editor.getSession();
                        that._renderer = that._editor.renderer;
                        that._renderer.setOption('fontSize', Number(that.$source.get('fontSize')));
                        that._editor.setOption('highlightActiveLine', that.$source.get('highlightActiveLine'));
                        that._session.setOption('wrap', that.$source.get('wrap'));
                        that._session.setOption('tabSize', Number(that.$source.get('tabSize')));
                        that._session.setOption('useSoftTabs', that.$source.get('useSoftTabs'));
                        that._editor.$blockScrolling = Infinity;
                    }
                };
            };
            AceDataModel.prototype.listenChanges = function () {
                var that = this;
                this.$source.listen(function ($key, $value) {
                    var newValue = $value;
                    switch ($key) {
                        case 'fontSize':
                            that._renderer.setOption('fontSize', Number(newValue));
                            break;
                        case 'showInvisibles':
                            that._renderer.setOption('showInvisibles', newValue);
                            break;
                        case 'highlightActiveLine':
                            that._editor.setOption('highlightActiveLine', newValue);
                            break;
                        case 'showGutter':
                            that._renderer.setOption('showGutter', newValue);
                            break;
                        case 'wrap':
                            that._session.setOption('wrap', newValue);
                            break;
                        case 'tabSize':
                            that._session.setOption('tabSize', Number(newValue));
                            break;
                        case 'useSoftTabs':
                            that._session.setOption('useSoftTabs', !newValue);
                            break;
                        case 'theme':
                            var gutter = that._renderer.getOption('showGutter');
                            that.aceOptions.theme = newValue;
                            that._renderer.setOption('showGutter', gutter);
                            break;
                    }
                });
            };
            return AceDataModel;
        })(dataSource.models.KVPDataModel);
        models.AceDataModel = AceDataModel;
    })(models = dataSource.models || (dataSource.models = {}));
})(dataSource || (dataSource = {}));

var dataSource;
(function (dataSource) {
    var types;
    (function (types) {
        var DataSource = (function () {
            function DataSource() {
            }
            return DataSource;
        })();
        types.DataSource = DataSource;
    })(types = dataSource.types || (dataSource.types = {}));
})(dataSource || (dataSource = {}));

/// <reference path="DataSource.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var dataSource;
(function (dataSource) {
    var types;
    (function (types) {
        var ConfigDataSource = (function (_super) {
            __extends(ConfigDataSource, _super);
            function ConfigDataSource() {
                _super.apply(this, arguments);
                this.subscribers = [];
                this.kvp = {};
            }
            ConfigDataSource.prototype.listen = function ($closure) {
                this.subscribers.push($closure);
                return this;
            };
            ConfigDataSource.prototype.touch = function ($key) {
                var _this = this;
                angular.forEach(this.subscribers, function (closure) {
                    if (false == closure($key, _this.kvp[$key]))
                        return;
                });
                return this;
            };
            ConfigDataSource.prototype.set = function ($key, $value) {
                var current = this.kvp[$key];
                if (current != $value) {
                    this.kvp[$key] = $value;
                    this.touch($key);
                }
                return this;
            };
            ConfigDataSource.prototype.returnSet = function ($key, $value) {
                this.set($key, $value);
                return this.kvp[$key];
            };
            ConfigDataSource.prototype.get = function ($key) {
                if (angular.isDefined(this.kvp[$key]))
                    return this.kvp[$key];
                console.error('Erreur dans le KVP la valeur ' + $key + ' n\'existe pas veuillez l\'initialiser a la mano');
            };
            ConfigDataSource.provideDefault = function ($source, data) {
                angular.forEach(data, function (value, key) {
                    $source.set(key, value);
                });
            };
            return ConfigDataSource;
        })(dataSource.types.DataSource);
        types.ConfigDataSource = ConfigDataSource;
    })(types = dataSource.types || (dataSource.types = {}));
})(dataSource || (dataSource = {}));

var osml;
(function (osml) {
    var controllers;
    (function (controllers) {
        var SidenavMenuController = (function () {
            function SidenavMenuController($scope, $element) {
                this.$element = $element;
                this.$scope = $scope;
            }
            SidenavMenuController.prototype.removeActive = function () {
                $(this.$element).removeClass('active');
            };
            return SidenavMenuController;
        })();
        controllers.SidenavMenuController = SidenavMenuController;
    })(controllers = osml.controllers || (osml.controllers = {}));
})(osml || (osml = {}));
