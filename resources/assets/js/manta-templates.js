angular.module("manta").run(['$templateCache', function(a) { a.put('osSidenavMenuItem.html', '<i class="{{icon}}"></i>\n' +
    '<p class="os-sidenav-menu-item--name">{{name}}</p>\n' +
    '<p class="os-sidenav-menu-item--description" ng-transclude></p>\n' +
    '\n' +
    '\n' +
    '');
	a.put('osSidenavMenu.html', '<p class="os-sidenav-menu--title">\n' +
    '    {{title}}\n' +
    '</p>\n' +
    '<div class="os-sidenav-menu--items" ng-transclude></div>\n' +
    '\n' +
    '\n' +
    '');
	a.put('osSidenavBrowserToolbox.html', '<div class="os-sidenav-browser-toolbox--search">\n' +
    '    <form>\n' +
    '        <input id="toolbox--search" type="search" ng-model="query.text"/>\n' +
    '\n' +
    '        <label for="toolbox--search"><i class="mdi-action-search"></i></label>\n' +
    '        <i class="mdi-navigation-close" ng-click="query.text = \'\'"></i>\n' +
    '    </form>\n' +
    '</div>\n' +
    '<div class="os-sidenav-browser-toolbox--breadcrumb">\n' +
    '    <i class="os-sidenav-browser-toolbox--breadcrumb-return mdi-hardware-keyboard-arrow-left" ng-click="returnClick()" ng-show="showReturn"></i>\n' +
    '    <i class="mdi-hardware-keyboard-arrow-right" ng-hide="showReturn"></i>\n' +
    '    <p class="os-sidenav-browser-toolbox--breadcrumb-directory">{{name}}</p>\n' +
    '\n' +
    '    <i class="os-sidenav-browser-toolbox--btn-add os-icon-plus-5" ng-click="add()"></i>\n' +
    '</div>\n' +
    '\n' +
    '');
	a.put('osSidenavBrowserItem.html', '<p class="os-sidenav-browser-item--name">{{name}}</p>\n' +
    '<i class="{{icon}}"></i>\n' +
    '<p class="os-sidenav-browser-item--description" ng-transclude></p>');
	a.put('osSidenavBrowser.html', '<p class="os-sidenav-browser--title">\n' +
    '    {{title}}\n' +
    '</p>\n' +
    '<os-sidenav-browser-toolbox show-return="history.length > 0" return-click="parent()"\n' +
    '                            name="{{directory}}" add="addFile()" search="fuctions">toolbox</os-sidenav-browser-toolbox>\n' +
    '<div class="os-sidenav-browser--items">\n' +
    '    <os-sidenav-browser-item name="{{value._name}}" icon="{{value.icon}}"\n' +
    '                             ng-repeat="value in current_directory |toArray| orderBy:[\'-isDirectory\',\'+_name\']| filter:{_name:searchQuery}"\n' +
    '                             ng-click="openFile(value._name)">{{value.info}}</os-sidenav-browser-item>\n' +
    '</div>\n' +
    '\n' +
    '\n' +
    '');
	a.put('osTaskbarUserpanel.html', '<li>\n' +
    '    <a class="os-taskbar-userpanel--btn">\n' +
    '        {{username}}\n' +
    '        <i class="mdi-social-person"></i>\n' +
    '    </a>\n' +
    '    <ul class="os-taskbar-userpanel--menu" ng-transclude>\n' +
    '\n' +
    '    </ul>\n' +
    '</li>\n' +
    '');
	a.put('osTaskbarItem.html', '<li ng-click="test()">\n' +
    '    <a>\n' +
    '        <i class="{{icon}}"></i>\n' +
    '        <p class="{{color}}" ng-transclude></p>\n' +
    '        <i ng-click="close()"\n' +
    '           class="os-taskbar-item--close mdi-navigation-close"></i>\n' +
    '    </a>\n' +
    '</li>');
	a.put('osTaskbar.html', '<nav>\n' +
    '    <div class="nav-wrapper" ng-transclude></div>\n' +
    '</nav>');
	a.put('osTabset.html', '<div class="row">\n' +
    '    <ul class="tabs" ng-class="{\'nav-stacked\': vertical, \'nav-justified\': justified}" ng-transclude></ul>\n' +
    '    <div class="tab-content">\n' +
    '        <div\n' +
    '             id="tabs{{$index}}"\n' +
    '             ng-repeat="tab in tabs"\n' +
    '             ng-show="tab.active"\n' +
    '             os-tab-content-transclude="tab">\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '');
	a.put('osTab.html', '<div class="tab-container" ng-class="{active: active, disabled: disabled}">\n' +
    '    <li class="tab">\n' +
    '        <a href ng-click="select()" tab-heading-transclude>{{heading}}</a>\n' +
    '        <i class=" tiny" ng-class="{\'mdi-navigation-close\':!modified,\'mdi-alert-error\':modified}" ng-click="close()"></i>\n' +
    '    </li>\n' +
    '</div>\n' +
    '');
	a.put('osSidenavItem.html', '<li class="os-sidenav-item--link">\n' +
    '    <a>\n' +
    '        <i ng-show="iconActive" class="mdi-navigation-arrow-back"></i>\n' +
    '        <i ng-hide="iconActive" class="{{icon}}"></i>\n' +
    '        <p class="{{color}}">{{label}}</p>\n' +
    '    </a>\n' +
    '</li>\n' +
    '<div class="os-sidenav-item--submenu" ng-transclude></div>\n' +
    '\n' +
    '\n' +
    '');
	a.put('osSidenav.html', '<ul class="os-sidenav--items {{color}}" ng-transclude>\n' +
    '\n' +
    '</ul>\n' +
    '<p class="os-sidenav--label">\n' +
    '    {{label}}\n' +
    '</p>\n' +
    '\n' +
    '');
	a.put('osSelect.html', '<label for="{{name}}" class="active" ng-transclude></label>\n' +
    '<select ng-model="model" id="{{name}}"\n' +
    '        ng-options="key as value for (key, value) in options ">\n' +
    '        <option value="" disabled>-- {{placeholder}} --</option>\n' +
    '</select>');
	a.put('osInput.html', '<label for="{{name}}" ng-transclude></label>\n' +
    '<input type="{{type}}" id="{{name}}" ng-model="model" ng-disabled="disabled">');
	 }]);